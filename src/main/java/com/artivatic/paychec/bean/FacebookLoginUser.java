package com.artivatic.paychec.bean;

 

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;

public class FacebookLoginUser  {

	/**
	 * 
	 */
	private String facebookId;
	private String emailId;

	private String firstName;
	private String lastName;
	private String deviceId;
	private String phoneNumber;
	private String Otp;
	
	private String profilePic;
	

	public FacebookLoginUser() {
		super();
		// TODO Auto-generated constructor stub
	}


	@NotNull
	@NotBlank
	public String getFacebookId() {
		return facebookId;
	}

	public void setFacebookId(String facebookId) {
		this.facebookId = facebookId;
	}

	@Email
	@NotNull
	@NotBlank
	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	@NotNull
	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

//	@NotNull
//	@NotBlank
	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	@NotNull
	@NotBlank
	public String getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}


	public String getOtp() {
		return Otp;
	}


	public void setOtp(String otp) {
		Otp = otp;
	}


	public String getProfilePic() {
		return profilePic;
	}


	public void setProfilePic(String profilePic) {
		this.profilePic = profilePic;
	}

 
	
	
	

}

