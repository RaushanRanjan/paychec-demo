package com.artivatic.paychec.bean;

import java.math.BigDecimal;
import java.util.Date;

import com.artivatic.paychec.entity.LoanType;
import com.artivatic.paychec.entity.User;

public class PaidLoanBean {

	private BigDecimal paidAmount;
//	private Date paidDate;
//	private String loanTypeId;
//	private String rateId;
	private String loanId;
	
	public BigDecimal getPaidAmount() {
		return paidAmount;
	}
	public void setPaidAmount(BigDecimal paidAmount) {
		this.paidAmount = paidAmount;
	}
	/*public String getLoanTypeId() {
		return loanTypeId;
	}
	public void setLoanTypeId(String loanTypeId) {
		this.loanTypeId = loanTypeId;
	}
	public String getRateId() {
		return rateId;
	}
	public void setRateId(String rateId) {
		this.rateId = rateId;
	}*/
	public String getLoanId() {
		return loanId;
	}
	public void setLoanId(String loanId) {
		this.loanId = loanId;
	}
	
	
}
