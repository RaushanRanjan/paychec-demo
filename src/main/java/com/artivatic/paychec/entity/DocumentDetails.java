package com.artivatic.paychec.entity;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.artivatic.paychec.document.DocumentType;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
@Table(name = "document_type")
public class DocumentDetails implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	private Long id;
//	private DocumentType documentType;
	private String nationalId;
	private String photo;
	private String  passport;
	private String  healthcertificate;
	private String paySlip;
	private String bankStatement;
	private String creditCard;
	private String  insurence;
	private String  emailId;
	private String  facebookId;
	private String linkedInId;
	
	
	
	
	
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	 public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
	  /* @JsonProperty("documentType")
		@ManyToOne(cascade = CascadeType.PERSIST,fetch = FetchType.LAZY)
		@NotFound(action=NotFoundAction.IGNORE)
		@JoinColumn(name="document_id")
//	@Column(name = "document_id")
	public DocumentType getDocumentType() {
		return documentType;
	}
	public void setDocumentType(DocumentType documentType) {
		this.documentType = documentType;
	}*/
	
	
	@Column(name = "national_id")
	public String getNationalId() {
		return nationalId;
	}
	public void setNationalId(String nationalId) {
		this.nationalId = nationalId;
	}
	@Column(name = "photo")
	public String getPhoto() {
		return photo;
	}
	public void setPhoto(String photo) {
		this.photo = photo;
	}
	@Column(name = "passport")
	public String getPassport() {
		return passport;
	}
	public void setPassport(String passport) {
		this.passport = passport;
	}
	@Column(name = "health_certificate")
	public String getHealthcertificate() {
		return healthcertificate;
	}
	public void setHealthcertificate(String healthcertificate) {
		this.healthcertificate = healthcertificate;
	}
	@Column(name = "pay_slip")
	public String getPaySlip() {
		return paySlip;
	}
	public void setPaySlip(String paySlip) {
		this.paySlip = paySlip;
	}
	@Column(name = "bank_statement")
	public String getBankStatement() {
		return bankStatement;
	}
	public void setBankStatement(String bankStatement) {
		this.bankStatement = bankStatement;
	}
	@Column(name = "credit_card")
	public String getCreditCard() {
		return creditCard;
	}
	public void setCreditCard(String creditCard) {
		this.creditCard = creditCard;
	}
	@Column(name = "insurence")
	public String getInsurence() {
		return insurence;
	}
	public void setInsurence(String insurence) {
		this.insurence = insurence;
	}
	@Column(name = "email_id")
	public String getEmailId() {
		return emailId;
	}
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	@Column(name = "facebook_id")
	public String getFacebookId() {
		return facebookId;
	}
	public void setFacebookId(String facebookId) {
		this.facebookId = facebookId;
	}
	@Column(name = "linkedIn_id")
	public String getLinkedInId() {
		return linkedInId;
	}
	public void setLinkedInId(String linkedInId) {
		this.linkedInId = linkedInId;
	}
	 
	
	/*
	@JsonIgnore
//	@OneToOne(fetch = FetchType.LAZY, mappedBy = "interestRate", cascade = CascadeType.ALL)
	@OneToMany(cascade = CascadeType.ALL,	fetch = FetchType.LAZY, mappedBy = "interestRate")*/
	
	
	   

}
