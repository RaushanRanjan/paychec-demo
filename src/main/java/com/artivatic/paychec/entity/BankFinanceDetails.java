package com.artivatic.paychec.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.artivatic.paychec.document.DocumentType;
import com.fasterxml.jackson.annotation.JsonIgnore;


@Entity
@Table(name = "bank_finanace_details")
public class BankFinanceDetails implements Serializable {
	
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Long id;
	 
	private String paySlip;
	private String bankStatement;
	private String creditCard;
	private String  insurence;
	
	private User user;
	
	
	

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	 public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
	
	
	@Column(name = "pay_slip")
	public String getPaySlip() {
		return paySlip;
	}
	public void setPaySlip(String paySlip) {
		this.paySlip = paySlip;
	}
	@Column(name = "bank_statement")
	public String getBankStatement() {
		return bankStatement;
	}
	public void setBankStatement(String bankStatement) {
		this.bankStatement = bankStatement;
	}
	@Column(name = "credit_card")
	public String getCreditCard() {
		return creditCard;
	}
	public void setCreditCard(String creditCard) {
		this.creditCard = creditCard;
	}
	@Column(name = "insurence")
	public String getInsurence() {
		return insurence;
	}
	public void setInsurence(String insurence) {
		this.insurence = insurence;
	}
	
	
	@JsonIgnore
//	@JsonProperty("status")
	 @OneToOne(fetch = FetchType.EAGER)
	 @JoinColumn(name="user_id"
//	 insertable=false,
//	 updatable = false, nullable = false
//	 ,columnDefinition="int(1) default 1"
	 )
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	 
}
