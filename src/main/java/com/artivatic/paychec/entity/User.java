package com.artivatic.paychec.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "user")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class User implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long id;
	private String facebookId;
	private String emailId;

	private String firstName;
	private String lastName;
	private String deviceId;
	private String phoneNumber;
	private String password;
	private String tokenId;
	private String profilePic;
	private String kycFrontPic;
	private String kycBackPic;
	private List<Loan> loan;
	private boolean status;
	private String Otp;
	private ResetPassword resetPasswords ;
//	private String bankstatement;
//	private String passport;
//	private String helathCertificate;
//	private String creditCard;
	private Date createdDate;

	private List<UserBankAccount> bankAccount;
//	private String payslipDocument;
	
	

	public User() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name = "facebook_id")
	public String getFacebookId() {
		return facebookId;
	}

	public void setFacebookId(String facebookId) {
		this.facebookId = facebookId;
	}

	@Column(name = "email_id")
	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	@Column(name = "first_name")
	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	@Column(name = "last_name")
	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	@Column(name = "device_id")
	public String getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}

	@Column(name = "phone_number")
	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	@Column(name = "password")
	public String getPassword() {
		return password;
	}

	@Column(name = "token_id")
	public String getTokenId() {
		return tokenId;
	}

	public void setTokenId(String tokenId) {
		this.tokenId = tokenId;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	@Column(name = "profile_pic")
	public String getProfilePic() {
		return profilePic;
	}

	public void setProfilePic(String profilePic) {
		this.profilePic = profilePic;
	}

	@Column(name = "kyc_front_pic")
	public String getKycFrontPic() {
		return kycFrontPic;
	}

	public void setKycFrontPic(String kycFrontPic) {
		this.kycFrontPic = kycFrontPic;
	}

	public User(Long id, String facebookId, String emailId, String firstName, String lastName, String deviceId,
			String phoneNumber, String password, String tokenId, String profilePic, String kycFrontPic, List<Loan> loan) {
		super();
		this.id = id;
		this.facebookId = facebookId;
		this.emailId = emailId;
		this.firstName = firstName;
		this.lastName = lastName;
		this.deviceId = deviceId;
		this.phoneNumber = phoneNumber;
		this.password = password;
		this.tokenId = tokenId;
		this.profilePic = profilePic;
		this.kycFrontPic = kycFrontPic;
		this.loan = loan;
	}

//	@JsonIgnore
	 // @Fetch(value = FetchMode.SUBSELECT) 
	@OneToMany(cascade = CascadeType.ALL,	fetch = FetchType.LAZY, mappedBy = "user")
	public List<Loan> getLoan() {
		return loan;
	}

	public void setLoan(List<Loan> loan) {
		this.loan = loan;
	}

	@Transient
	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}
	@Column(name = "otp")
	public String getOtp() {
		return Otp;
	}

	public void setOtp(String otp) {
		Otp = otp;
	}

	
	
	
/*	@OneToMany(cascade = CascadeType.ALL,	fetch = FetchType.LAZY, mappedBy = "user")
	public List<ResetPassword> getResetPasswords() {
		return resetPasswords;
	}

	public void setResetPasswords(List<ResetPassword> resetPasswords) {
		this.resetPasswords = resetPasswords;
	}*/
	
//	@OneToMany(cascade = CascadeType.ALL,	fetch = FetchType.LAZY, mappedBy = "user")
	 @OneToOne(mappedBy = "user", cascade = CascadeType.ALL, 
             fetch = FetchType.LAZY, optional = false)
	public ResetPassword getResetPasswords() {
		return resetPasswords;
	}

	public void setResetPasswords(ResetPassword resetPasswords) {
		this.resetPasswords = resetPasswords;
	}

	
	@OneToMany(cascade = CascadeType.ALL,	fetch = FetchType.LAZY, mappedBy = "user")
	public List<UserBankAccount> getBankAccount() {
		return bankAccount;
	}

	public void setBankAccount(List<UserBankAccount> bankAccount) {
		this.bankAccount = bankAccount;
	}

	
	/*@Column(name = "pay_slip_document")
	public String getPayslipDocument() {
		return payslipDocument;
	}

	public void setPayslipDocument(String payslipDocument) {
		this.payslipDocument = payslipDocument;
	}
	
	private String insurenceDocument;


	@Column(name = "insurence_document")
	public String getInsurenceDocument() {
		return insurenceDocument;
	}

	
	public void setInsurenceDocument(String insurenceDocument) {
		this.insurenceDocument = insurenceDocument;
	}*/

	@Column(name = "kyc_back_pic")
	public String getKycBackPic() {
		return kycBackPic;
	}

	public void setKycBackPic(String kycBackPic) {
		this.kycBackPic = kycBackPic;
	}
	
	

/*
	@Column(name = "bank_statement_doc")
	public String getBankstatement() {
		return bankstatement;
	}

	public void setBankstatement(String bankstatement) {
		this.bankstatement = bankstatement;
	}

	@Column(name = "passport_doc")
	public String getPassport() {
		return passport;
	}

	public void setPassport(String passport) {
		this.passport = passport;
	}

	@Column(name = "health_document")
	public String getHelathCertificate() {
		return helathCertificate;
	}

	public void setHelathCertificate(String helathCertificate) {
		this.helathCertificate = helathCertificate;
	}

	@Column(name = "credit_card_document")
	public String getCreditCard() {
		return creditCard;
	}

	public void setCreditCard(String creditCard) {
		this.creditCard = creditCard;
	}*/
	
	
	private BankFinanceDetails bankFinanceDetails;



	 @OneToOne(mappedBy = "user", cascade = CascadeType.ALL, 
             fetch = FetchType.LAZY, optional = false)
	public BankFinanceDetails getBankFinanceDetails() {
		return bankFinanceDetails;
	}

	public void setBankFinanceDetails(BankFinanceDetails bankFinanceDetails) {
		this.bankFinanceDetails = bankFinanceDetails;
	}
	
	
	private KycDocumentDetails kycDocumentDetails;


	 @OneToOne(mappedBy = "user", cascade = CascadeType.ALL, 
             fetch = FetchType.LAZY, optional = false)
	public KycDocumentDetails getKycDocumentDetails() {
		return kycDocumentDetails;
	}

	public void setKycDocumentDetails(KycDocumentDetails kycDocumentDetails) {
		this.kycDocumentDetails = kycDocumentDetails;
	}
	
	
	private List<DocumentImage>  documentImage;
	
	
	
	
	
	
	
	
	@OneToMany(cascade = CascadeType.ALL,	fetch = FetchType.LAZY, mappedBy = "user")
	public List<DocumentImage> getDocumentImage() {
		return documentImage;
	}

	public void setDocumentImage(List<DocumentImage> documentImage) {
		this.documentImage = documentImage;
	}

//	private SocialInformation socialInformation;



	/*@OneToOne(mappedBy = "user", cascade = CascadeType.ALL, fetch = FetchType.EAGER,orphanRemoval = true)
	
//	 @OneToOne(mappedBy = "user", cascade = CascadeType.ALL,   fetch = FetchType.LAZY, optional = false)
	public SocialInformation getSocialInformation() {
		return socialInformation;
	}

	public void setSocialInformation(SocialInformation socialInformation) {
		this.socialInformation = socialInformation;
	}*/
	
	
	
	
	 
	
	private List<PaidLoan> paidLoan;


	@OneToMany(cascade = CascadeType.ALL,	fetch = FetchType.LAZY, mappedBy = "user")
	public List<PaidLoan> getPaidLoan() {
		return paidLoan;
	}

	public void setPaidLoan(List<PaidLoan> paidLoan) {
		this.paidLoan = paidLoan;
	}
	
	private boolean loogedIn;

	
	@Column(name = "logged_in")
	public boolean isLoogedIn() {
		return loogedIn;
	}

	public void setLoogedIn(boolean loogedIn) {
		this.loogedIn = loogedIn;
	}

	@JsonFormat(shape=JsonFormat.Shape.STRING, pattern="dd-MM-yyyy", timezone="IST")
	@Column(name = "created_date")
	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	
	private boolean verified;



	@Column(name = "verify_by_admin")
	public boolean isVerified() {
		return verified;
	}

	public void setVerified(boolean verified) {
		this.verified = verified;
	}
	
	

	
	
	
	
	

}
