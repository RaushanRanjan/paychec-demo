package com.artivatic.paychec.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;




@Entity
@Table(name = "social_details")
public class SocialInformation implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	
	private Long id;
	private String emailId;
	private String facebookProfile;
	private String  linkedInProfile;
	
    private User user;

    
    @Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name = "email_id")
	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	@Column(name = "facebook_id")
	public String getFacebookProfile() {
		return facebookProfile;
	}

	public void setFacebookProfile(String facebookProfile) {
		this.facebookProfile = facebookProfile;
	}

	@Column(name = "linkedIn_id")
	public String getLinkedInProfile() {
		return linkedInProfile;
	}

	public void setLinkedInProfile(String linkedInProfile) {
		this.linkedInProfile = linkedInProfile;
	}

//	@Column(name = "user_id")
	@JsonIgnore
//	@JsonProperty("status")
	 @OneToOne(fetch = FetchType.EAGER) 
	@JoinColumn(name="user_id"
//	 insertable=false,
//	 updatable = false, nullable = false
//	 ,columnDefinition="int(1) default 1"
	 )
	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
    
    
    
    
    
    
    
    

}
