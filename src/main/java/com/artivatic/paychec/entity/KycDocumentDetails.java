package com.artivatic.paychec.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;


@Entity
@Table(name = "kyc_document_details")
public class KycDocumentDetails implements Serializable{
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long id;
	private String nationalId;
	private String photo;
	private String  passport;
	private String  healthcertificate;
	
    private User user;
	
	
	

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	 public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
	
	
	
	
	
	@Column(name = "national_id")
	public String getNationalId() {
		return nationalId;
	}
	public void setNationalId(String nationalId) {
		this.nationalId = nationalId;
	}
	@Column(name = "photo")
	public String getPhoto() {
		return photo;
	}
	public void setPhoto(String photo) {
		this.photo = photo;
	}
	@Column(name = "passport")
	public String getPassport() {
		return passport;
	}
	public void setPassport(String passport) {
		this.passport = passport;
	}
	@Column(name = "health_certificate")
	public String getHealthcertificate() {
		return healthcertificate;
	}
	public void setHealthcertificate(String healthcertificate) {
		this.healthcertificate = healthcertificate;
	}

	@JsonIgnore
//	@JsonProperty("status")
	 @OneToOne(fetch = FetchType.EAGER)
	 @JoinColumn(name="user_id"
//	 insertable=false,
//	 updatable = false, nullable = false
//	 ,columnDefinition="int(1) default 1"
	 )
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	
	
	
}
