package com.artivatic.paychec.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "user_otp")
// @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class ResetPassword implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long id;
	// private String time;
	private String otp;
	private User user;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	private Date time;

	@Column(name = "time")
	@Temporal(TemporalType.TIME)
	public Date getTime() {
		return time;
	}

	public void setTime(Date time) {
		this.time = time;
	}

	private Date date;

	@Column(name = "date")
	// @Temporal(TemporalType.DATE)
	@Temporal(TemporalType.TIMESTAMP)
	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	/*
	 * public String getTime() { return time; } public void setTime(String time) {
	 * this.time = time; }
	 */
	@Column(name = "otp")
	public String getOtp() {
		return otp;
	}

	public void setOtp(String otp) {
		this.otp = otp;
	}

	// @Column(name = "id")
	@JsonIgnore
	// @ManyToOne(cascade = CascadeType.PERSIST,fetch = FetchType.LAZY)
	// @NotFound(action=NotFoundAction.IGNORE)
	// @JoinColumn(name="user_id")

	// @JsonProperty("status")
	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "user_id"
	// insertable=false,
	// updatable = false, nullable = false
	// ,columnDefinition="int(1) default 1"
	)
	// @OneToOne(fetch = FetchType.EAGER)
	// @JoinColumn(name="invitation_response_id",insertable=false, updatable =
	// false, nullable = false,columnDefinition="int(1) default 1")
	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	/*
	 * @JsonIgnore
	 * 
	 * @ManyToOne(cascade = CascadeType.PERSIST,fetch = FetchType.LAZY)
	 * 
	 * @NotFound(action=NotFoundAction.IGNORE)
	 * 
	 * @JoinColumn(name="user_id") public User getUser() { return user; } public
	 * void setUser(User user) { this.user = user; }
	 */
	public ResetPassword() {
		super();
		// TODO Auto-generated constructor stub
	}

	private String emailId;

	@Column(name = "email")
	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	/*
	 * private Integer isverify;
	 * 
	 * @Column(name = "verified_user") public Integer getIsverify() { return
	 * isverify; } public void setIsverify(Integer isverify) { this.isverify =
	 * isverify; }
	 */

	private Boolean verify;

	// @Column(name = "verified_user")
	//// @Type(type = "org.hibernate.type.NumericBooleanType")
	@Column(name = "verified_user", nullable = false, columnDefinition = "BOOLEAN DEFAULT false")
	public Boolean getVerify() {
		return verify;
	}

	public void setVerify(Boolean verify) {
		this.verify = verify;
	}

}
