package com.artivatic.paychec.firebase;

import java.lang.management.ManagementFactory;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import java.util.Set;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.management.AttributeNotFoundException;
import javax.management.InstanceNotFoundException;
import javax.management.MBeanException;
import javax.management.MBeanServer;
import javax.management.MalformedObjectNameException;
import javax.management.ObjectName;
import javax.management.Query;
import javax.management.ReflectionException;

import com.artivatic.paychec.entity.User;

public class IpAddress {

	public static void sendMail(User user, String txtmessage, String title) {
		final String username = "raushan.jnv@gmail.com";
		final String password = "636317636317636317";

		Properties props = new Properties();
		props.put("mail.smtp.auth", "true");
		// props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.host", "smtp.gmail.com");
		props.put("mail.smtp.port", "587");
		// props.put("mail.smtp.host", "smtp.gmail.com");
		props.put("mail.smtp.socketFactory.port", "465");
		props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
		// props.put("mail.smtp.auth", "true");
		// props.put("mail.smtp.port", "465");

		Session session = Session.getInstance(props, new javax.mail.Authenticator() {
			protected javax.mail.PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(username, password);
			}
		});

		try {

			Message message = new MimeMessage(session);
			// message.setFrom(new InternetAddress("raushan.jnv@gmail.com"));
			message.setFrom(new InternetAddress("PayChec"));
			message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(user.getEmailId()));
			message.setSubject(title);
			// message.setText(txtmessage);
			StringBuilder html = new StringBuilder();
			html.append("<!DOCTYPE html><html><head><meta charset=\'utf-8\'></head><body >");

			html.append(
					"<div style ='background:white; height:450px; width:600px; position: fixed;top: 40%;left: 50%;-webkit-transform: translate(-50%, -50%);  transform: translate(-50%, -50%);'>");
			html.append(
					"<div style='background-image: url(http://185.78.163.41:8080/paychec/logo_dummy@2x.png); height: 430px; width: 600px;'>");
			html.append("<div style = 'padding-top:50px;'>");
			html.append("<p style='padding-left: 20px;'>Dear " + user.getFirstName() + ",</p>  ");
			html.append("<p style='margin-left: 3.5em'>   Your Paychec OTP " + txtmessage
					+ " (it will Expire in 10 minutes)  </p> </div> </div>");

			html.append(
					"<div style ='background:#4286f4; height:30px;'><p style='display:block;text-align: center;margin:auto; padding-top:5px;'>&copy; Copyright 2018 - all rights reserved to www.paychec.com </p> </div>");
			html.append(" </div>");

			html.append(" </body></html>");
			message.setContent(html.toString(), "text/html");

			Transport.send(message);

			System.out.println("Done");

		} catch (MessagingException e) {
			throw new RuntimeException(e);
		}
	}

	public static List<String> getEndPoints()
			throws MalformedObjectNameException, NullPointerException, UnknownHostException, AttributeNotFoundException,
			InstanceNotFoundException, MBeanException, ReflectionException {
		MBeanServer mbs = ManagementFactory.getPlatformMBeanServer();
		Set<ObjectName> objs = mbs.queryNames(new ObjectName("*:type=Connector,*"),
				Query.match(Query.attr("protocol"), Query.value("HTTP/1.1")));
		String hostname = InetAddress.getLocalHost().getHostName();
		InetAddress[] addresses = InetAddress.getAllByName(hostname);
		ArrayList<String> endPoints = new ArrayList<String>();
		for (Iterator<ObjectName> i = objs.iterator(); i.hasNext();) {
			ObjectName obj = i.next();
			String scheme = mbs.getAttribute(obj, "scheme").toString();
			String port = obj.getKeyProperty("port");
			for (InetAddress addr : addresses) {
				String host = addr.getHostAddress();
				String ep = scheme + "://" + host + ":" + port;
				endPoints.add(ep);
			}
		}
		return endPoints;
	}

}
