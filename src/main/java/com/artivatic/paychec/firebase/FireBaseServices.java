package  com.artivatic.paychec.firebase;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import org.json.JSONException;
import org.json.simple.JSONObject;

import com.google.gson.JsonObject;
 

public class FireBaseServices {
	
		public final static String AUTH_KEY_FCM = "AIzaSyCNkY8ehRWx_dc9skdyUlcRUVsbv-kLwXM";
				
		public final static String API_URL_FCM = "https://fcm.googleapis.com/fcm/send";
		
		//-----------------------------------working for notification message------------------------------------
		
		public static String sendFCMNotification(String message) {
			System.out.println("FCM    connection");
			
			//JsonObject jsonObject = new JsonObject(message);
	        System.out.println("message : "+message);
			String authKey = "key="+AUTH_KEY_FCM;   // You FCM AUTH key
			String FMCurl = API_URL_FCM;  
			HttpURLConnection urlConnection = null;
			 StringBuilder sb = new StringBuilder();
		    try {
		    	
		    	URL url = new URL(FMCurl);
		        urlConnection = (HttpURLConnection) url.openConnection();
		        urlConnection.setDoOutput(true);
		        urlConnection.setRequestMethod("POST");
		        urlConnection.setUseCaches(false);
		        urlConnection.setConnectTimeout(50000);
		        urlConnection.setReadTimeout(50000);
		        urlConnection.setRequestProperty("Content-Type", "application/json");
		        urlConnection.setRequestProperty("Authorization",authKey);
		        urlConnection.connect();
		        //You Can also Create JSONObject here 
		        OutputStreamWriter out = new OutputStreamWriter(urlConnection.getOutputStream());
		        out.write(message.toString());// here i sent the parameter
		        out.close();
		        int HttpResult = urlConnection.getResponseCode();
		        if (HttpResult == HttpURLConnection.HTTP_OK) {
		            BufferedReader br = new BufferedReader(new InputStreamReader(urlConnection.getInputStream(), "utf-8"));
		            String line = null;
		            while ((line = br.readLine()) != null) {
		                sb.append(line + "\n");
		            }
		            br.close();
			        System.out.println("Builder : " +  sb.toString());
		            return sb.toString();
		        } else {
			        System.out.println("Response Code : " + urlConnection.getResponseMessage());
		        }
		    } catch (MalformedURLException e) {
		        e.printStackTrace();
		    } catch (IOException e) {
		        e.printStackTrace();
		    } catch (JSONException e) {
		        e.printStackTrace();
		    } finally {
		        if (urlConnection != null)
		            urlConnection.disconnect();
		    }
		    return null;
		}
			
		public static void main(String []args) throws Exception{
			String serverKey = "AIzaSyCNkY8ehRWx_dc9skdyUlcRUVsbv-kLwXM";
			String authKey = AUTH_KEY_FCM;   // You FCM AUTH key
			String FMCurl = API_URL_FCM;    
			String userDeviceIdKey = "d3t-IQmw9gE:APA91bE4RwZFdc91_o4XT1PIsBhBsYHay_Znuy8RhNfzCeNianUvW2N_pS6WxzqJ96WdbPxnxwQxEAC66dQoyG79uKk_rfP3GzmyTkr4LF89npIRjq9k_8q1JC6IN71jMxud3OV9Xdob";
 
		
			URL url = new URL(FMCurl);
			HttpURLConnection conn = (HttpURLConnection)url.openConnection();
			
			conn.setUseCaches(false);
			conn.setDoInput(true);
			conn.setDoOutput(true);
			conn.setRequestMethod("POST");
			conn.setRequestProperty("Authorization","key="+authKey);
			conn.setRequestProperty("Content-Type","application/json");
			conn.setConnectTimeout(50000);
			conn.setReadTimeout(50000);
			JSONObject json = new JSONObject();
	
			json.put("registration_ids",userDeviceIdKey);
			JSONObject info = new JSONObject();
			info.put("title", "Notificatoin Title");   
			info.put("body", "Hello Test notification"); 
			json.put("data", info);

			OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
 			wr.write(json.toString());
			wr.flush();
			 conn.getInputStream();
			
			 
			
		}
		
		
 
	
}
