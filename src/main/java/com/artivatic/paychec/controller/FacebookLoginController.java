package com.artivatic.paychec.controller;

import java.security.Key;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.artivatic.paychec.bean.FacebookLoginUser;
import com.artivatic.paychec.dao.PayCheckDao;
import com.artivatic.paychec.entity.DocumentImage;
import com.artivatic.paychec.entity.DocumentSubType;
import com.artivatic.paychec.entity.ResetPassword;
import com.artivatic.paychec.entity.User;
import com.artivatic.paychec.firebase.IpAddress;
import com.artivatic.paychec.util.GenerateOTP;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.impl.crypto.MacProvider;

@Controller
public class FacebookLoginController {

	@Autowired
	PayCheckDao payCheckDao;

	private static final String CODE_ALPHA_NUMERICS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";

	@RequestMapping(value = "/fblogin", method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> facebookLogin(@RequestBody FacebookLoginUser fuser) {
		// String title = "OTP";
		Map<String, Object> status = new HashMap<String, Object>();
		User matchedUser = payCheckDao.findUserByfacebookId(fuser.getFacebookId());
		// StringBuilder otp = GenerateOTP.generateOtp();//// Generating otp with alpha
		// numeric

		/*
		 * checking existing facebook id
		 */
		// if (payCheckDao.findUserByfacebookId(fuser.getFacebookId()) != null) {
		if (matchedUser != null) {
			status.put("status", "true");
			status.put("id", matchedUser.getId());
			status.put("firstName", matchedUser.getFirstName());
			status.put("tokenId", matchedUser.getTokenId());
			if (matchedUser.getKycFrontPic() != null && matchedUser.getKycBackPic() != null) {
				status.put("idProof", "true");// need to check
			} else {

				status.put("idProof", "false");
			}
			status.put("verify", "true");
			// User checkedUser = matchedUser;
			System.out.println(matchedUser.isLoogedIn());
			if (!matchedUser.isLoogedIn()) {
				matchedUser.setLoogedIn(true);
				User checkedUser = payCheckDao.mergeUser(matchedUser);
				status.put("isFacebook", checkedUser.isLoogedIn());
			} else {
				status.put("isFacebook", matchedUser.isLoogedIn());
			}
			status.put("profilePic", matchedUser.getProfilePic());

		} else {
			/* creating new facebook records */
			System.out.println("***********created new Records************");
			User user = new User();
			user.setFacebookId(fuser.getFacebookId());
			user.setFirstName(fuser.getFirstName());
			user.setLastName(fuser.getLastName());
			user.setPhoneNumber(fuser.getPhoneNumber());
			user.setEmailId(fuser.getEmailId());
			user.setDeviceId(fuser.getDeviceId());
			user.setProfilePic(fuser.getProfilePic());
			user.setLoogedIn(true);// facebook login true
			user.setCreatedDate(new Date());

			if (payCheckDao.findUserByEmail(user) != null) {
				status.put("status", "failure");
				status.put("message", "Email " + user.getEmailId() + "  allready exist");
			} else if (payCheckDao.findUserByPhoneNumber(user) != null) {
				status.put("status", "failure");
				status.put("message", "Phone nember " + user.getPhoneNumber() + "  allready exist");

			} else {

				String token = createToken(user);// creating token
				user.setTokenId(token);
				User cUser = payCheckDao.mergeUser(user);
				if (cUser != null) {// saving document details
					DocumentSubType documentSubType = null;
					if (cUser.getEmailId() != null || !cUser.getEmailId().equals("")) {
						documentSubType = payCheckDao.findDocumentSubTypeByName("Email");
						DocumentImage di = new DocumentImage();
						di.setDcumentType(documentSubType.getDocumentType());
						di.setDocumentSubType(documentSubType);
						di.setUser(cUser);
						di.setImageSource(cUser.getEmailId());
						payCheckDao.saveDocumentImage(di);
					}
					if (cUser.getFacebookId() != null || !cUser.getFacebookId().equals("")) {
						documentSubType = payCheckDao.findDocumentSubTypeByName("Facebook");
						DocumentImage di = new DocumentImage();
						di.setDcumentType(documentSubType.getDocumentType());
						di.setDocumentSubType(documentSubType);
						di.setUser(cUser);
						di.setImageSource(cUser.getFacebookId());
						payCheckDao.saveDocumentImage(di);
					}
				}
				status.put("status", "true");
				status.put("firstName", user.getFirstName());
				status.put("id", cUser.getId());
				status.put("tokenId", token);
				status.put("verify", "true");
				status.put("message", "Account  verfied");
				status.put("idProof", "false");
				status.put("isFacebook", cUser.isLoogedIn());
				status.put("profilePic", cUser.getProfilePic());
			}
		}
		return status;

	}

	public StringBuilder generateToken() {

		Random generator = new Random();
		StringBuilder numericCode = new StringBuilder(4);
		while (numericCode.length() < 6) {
			char nextChar = CODE_ALPHA_NUMERICS.charAt(generator.nextInt(CODE_ALPHA_NUMERICS.length()));
			numericCode.append(nextChar);
		}
		return numericCode;
	}

	public String createToken(User matchedUser) {
		Key key = MacProvider.generateKey();
		String compactJws = Jwts.builder().setSubject(matchedUser.getEmailId()).signWith(SignatureAlgorithm.HS512, key)
				.compact();
		return compactJws;

	}

}
