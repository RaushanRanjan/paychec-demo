package com.artivatic.paychec.controller;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.artivatic.paychec.dao.PayCheckDao;
import com.artivatic.paychec.entity.ResetPassword;
import com.artivatic.paychec.entity.User;
import com.artivatic.paychec.firebase.IpAddress;
import com.artivatic.paychec.util.GenerateOTP;

@Controller
public class SignInController {
	private static final String CODE_ALPHA_NUMERICS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";

	@Autowired
	PayCheckDao payCheckDao;

	@RequestMapping(value = "/signIn", method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> signInUser(@RequestBody User user) {
		System.out.println("sign User  " + user.getEmailId() + " " + user.getPassword());
		Map<String, Object> status = new HashMap<String, Object>();
		if (user.getEmailId() != null && user.getEmailId() != "") {
			User matchedUser = payCheckDao.findUserByEmail(user);

			if (matchedUser != null) {
				// if(matchedUser.getFacebookId() != null) {
				if (matchedUser.isLoogedIn()) {
					if (matchedUser.getPassword() == null) {

						status.put("status", "false");
						if (matchedUser.getKycFrontPic() != null && matchedUser.getKycBackPic() != null) {
							status.put("idProof", "true");// need to check
						} else {
							status.put("idProof", "false");
						}
						status.put("verify", "false");
						status.put("message", "You earlier logged in with Facebook.Please reset password");
						status.put("isFacebook", matchedUser.isLoogedIn());
						status.put("profilePic", matchedUser.getProfilePic());

					} else {
						if (user.getPassword().equals(matchedUser.getPassword())) {
							status = verifiedUser(matchedUser);
							/*
							 * if (matchedUser.getResetPasswords().getVerify()) { status.put("status",
							 * "true"); status.put("id", matchedUser.getId()); status.put("firstName",
							 * matchedUser.getFirstName()); status.put("tokenId", matchedUser.getTokenId());
							 * status.put("message", "Account verified"); status.put("verify", "true");
							 * status.put("isFacebook", matchedUser.isLoogedIn()); status.put("profilePic",
							 * matchedUser.getProfilePic()); if (matchedUser.getKycFrontPic() != null &&
							 * matchedUser.getKycBackPic() != null) { status.put("idProof", "true"); } else
							 * { status.put("idProof", "false"); } }else {
							 * 
							 * StringBuilder otp = GenerateOTP.generateOtp(); ResetPassword resPass =
							 * matchedUser.getResetPasswords(); resPass.setOtp(otp.toString());
							 * resPass.setDate(new Date()); String title = "OTP";
							 * payCheckDao.updateOtp(resPass); IpAddress.sendMail(matchedUser,
							 * otp.toString(), title);
							 * 
							 * status.put("id", matchedUser.getId()); status.put("firstName",
							 * matchedUser.getFirstName()); status.put("tokenId", matchedUser.getTokenId());
							 * status.put("idProof", "false"); status.put("status", "true");
							 * status.put("verify", "false"); status.put("message",
							 * "Account not verfied ,Please check mail for OTP"); status.put("isFacebook",
							 * matchedUser.isLoogedIn()); status.put("profilePic",
							 * matchedUser.getProfilePic());
							 * 
							 * }
							 */
						} else {
							status.put("status", "false");
							status.put("message", "Password  not matched");
							status.put("verify", "false");
						}

						/*
						 * status.put("status", "true"); if (matchedUser.getKycFrontPic() != null &&
						 * matchedUser.getKycBackPic() != null) { status.put("idProof", "true");// need
						 * to check } else { status.put("idProof", "false"); } status.put("verify",
						 * "true"); //matchedUser.getResetPasswords().getVerify() //
						 * status.put("message",
						 * "You earlier logged in with Facebook.Please reset password");
						 * status.put("message", "Account verified");// need to check
						 * status.put("isFacebook", matchedUser.isLoogedIn()); status.put("profilePic",
						 * matchedUser.getProfilePic());
						 */

					}

				} else {

					System.out.println("Not facebook account ,Logged in with Email");

					if (user.getPassword().equals(matchedUser.getPassword())) {
						status = verifiedUser(matchedUser);
						/*
						 * if (matchedUser.getResetPasswords().getVerify()) {
						 * 
						 * status.put("status", "true"); status.put("id", matchedUser.getId());
						 * status.put("firstName", matchedUser.getFirstName()); status.put("tokenId",
						 * matchedUser.getTokenId()); status.put("message", "Account verified");
						 * status.put("verify", "true"); status.put("isFacebook",
						 * matchedUser.isLoogedIn()); status.put("profilePic",
						 * matchedUser.getProfilePic()); if (matchedUser.getKycFrontPic() != null &&
						 * matchedUser.getKycBackPic() != null) { status.put("idProof", "true"); } else
						 * { status.put("idProof", "false"); } }else { StringBuilder otp =
						 * GenerateOTP.generateOtp(); ResetPassword resPass =
						 * matchedUser.getResetPasswords(); resPass.setOtp(otp.toString());
						 * resPass.setDate(new Date()); String title = "OTP";
						 * payCheckDao.updateOtp(resPass); IpAddress.sendMail(matchedUser,
						 * otp.toString(), title);
						 * 
						 * status.put("id", matchedUser.getId()); status.put("firstName",
						 * matchedUser.getFirstName()); status.put("tokenId", matchedUser.getTokenId());
						 * status.put("idProof", "false"); status.put("status", "true");
						 * status.put("verify", "false"); status.put("message",
						 * "Account not verfied ,Please check mail for OTP"); status.put("isFacebook",
						 * matchedUser.isLoogedIn()); status.put("profilePic",
						 * matchedUser.getProfilePic());
						 * 
						 * }
						 */
					} else {
						status.put("status", "false");
						status.put("message", "Password  not matched");
						status.put("verify", "false");
					}
				}

			} else {
				status.put("status", "false");
				status.put("idProof", "false");
				status.put("verify", "false");
				status.put("message", user.getEmailId() + " not registered");
			}
		}
		return status;

	}

	public StringBuilder generateToken() {

		Random generator = new Random();
		StringBuilder numericCode = new StringBuilder(4);
		while (numericCode.length() < 6) {
			char nextChar = CODE_ALPHA_NUMERICS.charAt(generator.nextInt(CODE_ALPHA_NUMERICS.length()));
			numericCode.append(nextChar);
		}
		return numericCode;
	}

	public Map<String, Object> verifiedUser(User matchedUser) {
		Map<String, Object> status = new HashMap<String, Object>();

		if (matchedUser.getResetPasswords().getVerify()) {

			status.put("status", "true");
			status.put("id", matchedUser.getId());
			status.put("firstName", matchedUser.getFirstName());
			status.put("tokenId", matchedUser.getTokenId());
			status.put("message", "Account verified");
			status.put("verify", "true");
			status.put("isFacebook", matchedUser.isLoogedIn());
			status.put("profilePic", matchedUser.getProfilePic());
			if (matchedUser.getKycFrontPic() != null && matchedUser.getKycBackPic() != null) {
				status.put("idProof", "true");
			} else {
				status.put("idProof", "false");
			}
		} else {
			StringBuilder otp = GenerateOTP.generateOtp();
			ResetPassword resPass = matchedUser.getResetPasswords();
			resPass.setOtp(otp.toString());
			resPass.setDate(new Date());
			 
			String title = "OTP";
			payCheckDao.updateOtp(resPass);
			// IpAddress.sendMail(matchedUser, txtmessage, title);
			IpAddress.sendMail(matchedUser, otp.toString(), title);

			status.put("id", matchedUser.getId());
			status.put("firstName", matchedUser.getFirstName());
			status.put("tokenId", matchedUser.getTokenId());
			status.put("idProof", "false");
			status.put("status", "true");
			status.put("verify", "false");
			status.put("message", "Account not verfied ,Please check mail for OTP");
			status.put("isFacebook", matchedUser.isLoogedIn());
			status.put("profilePic", matchedUser.getProfilePic());

		}

		return status;
	}

}
