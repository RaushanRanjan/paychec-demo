package com.artivatic.paychec.controller;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.Key;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.io.FilenameUtils;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.artivatic.paychec.bean.BankDetails;
import com.artivatic.paychec.bean.FacebookLoginUser;
import com.artivatic.paychec.bean.SignUpUserBean;
import com.artivatic.paychec.dao.PayCheckDao;
import com.artivatic.paychec.document.StaticIP;
import com.artivatic.paychec.entity.DocumentImage;
import com.artivatic.paychec.entity.DocumentSubType;
import com.artivatic.paychec.entity.DocumentType;
import com.artivatic.paychec.entity.Loan;
import com.artivatic.paychec.entity.NotificationEnum;
import com.artivatic.paychec.entity.ResetPassword;
import com.artivatic.paychec.entity.User;
import com.artivatic.paychec.exception.SessionExpiredException;
import com.artivatic.paychec.firebase.FireBaseServices;
import com.artivatic.paychec.firebase.IpAddress;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.impl.crypto.MacProvider;

@Controller
public class UserController {

	@Autowired
	PayCheckDao payCheckDao;
	private static final String CODE_NUMERICS = "1234567890";

	private static final String CODE_ALPHA_NUMERICS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
	// String SALTCHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";


	@RequestMapping(value = "/fblogin1111", method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> createUser(@RequestBody FacebookLoginUser fuser) {
		String title = "OTP";
		Map<String, Object> status = new HashMap<String, Object>();
		User matchedUser = payCheckDao.findUserByfacebookId(fuser.getFacebookId());
		StringBuilder otp = generateToken();// Generating otp with alph numeric
		System.out.println("^^^^^^^^^^^^^^^^^^^^^^^^^^^^6");

		/*
		 * checking existing facebook id
		 */
		// if (payCheckDao.findUserByfacebookId(fuser.getFacebookId()) != null) {
		if (matchedUser != null) {
			System.out.println("****************************");
			if (matchedUser.getResetPasswords().getVerify()) {// after firstlogin it always verified
				System.out.println("&&&&&&&&&&&&&&&&&&&&&&&7");
				status.put("status", "true");
				status.put("id", matchedUser.getId());
				status.put("firstName", matchedUser.getFirstName());
				status.put("tokenId", matchedUser.getTokenId());
				if (matchedUser.getKycFrontPic() != null && matchedUser.getKycBackPic() != null) {
					status.put("idProof", "true");// need to check
				} else {

					status.put("idProof", "false");
				}
				status.put("verify", "true");
				// status.put("verify", matchedUser.getResetPasswords().getVerify());

			} else {
				/* this code is not using in currently */

				ResetPassword resPass = matchedUser.getResetPasswords();
				resPass.setOtp(otp.toString());
				resPass.setDate(new Date());
				payCheckDao.updateOtp(resPass);
				String txtmessage = ("Dear " + matchedUser.getFirstName() + "," + "\n\n Your New OTP is \n"
						+ otp.toString());
				// IpAddress.sendMail(matchedUser, txtmessage, title);
				IpAddress.sendMail(matchedUser, otp.toString(), title);

				status.put("id", matchedUser.getId());
				status.put("firstName", matchedUser.getFirstName());
				status.put("tokenId", matchedUser.getTokenId());

				status.put("status", "true");
				status.put("verify", "true");
				// status.put("message", "Account not verfied");
				status.put("message", "Account  verfied");
				if (matchedUser.getKycFrontPic() != null && matchedUser.getKycBackPic() != null) {
					status.put("idProof", "true");// need to check
				} else {

					status.put("idProof", "false");
				}

			}

		} else {
			/* creating new facebook records */
			System.out.println("***********************");
			User user = new User();
			user.setFacebookId(fuser.getFacebookId());
			user.setFirstName(fuser.getFirstName());
			user.setLastName(fuser.getLastName());
			user.setPhoneNumber(fuser.getPhoneNumber());
			user.setEmailId(fuser.getEmailId());
			user.setDeviceId(fuser.getDeviceId());
			user.setProfilePic(fuser.getProfilePic());

			if (payCheckDao.findUserByEmail(user) != null) {
				status.put("status", "failure");
				status.put("message", "Email " + user.getEmailId() + "  allready exist");
			} else if (payCheckDao.findUserByPhoneNumber(user) != null) {
				status.put("status", "failure");
				status.put("message", "Phone nember " + user.getPhoneNumber() + "  allready exist");

			} else {
				ResetPassword resetPassword = new ResetPassword();
				resetPassword.setUser(user);
				resetPassword.setTime(new Date());
				resetPassword.setOtp(otp.toString());
				// resetPassword.setOtp(String.valueOf(1)); //setting otp true for facebook
				resetPassword.setEmailId(user.getEmailId());
				resetPassword.setDate(new Date());
				resetPassword.setVerify(true);
				user.setResetPasswords(resetPassword);

				String token = createToken(user);// creating token
				user.setTokenId(token);
				User cUser = payCheckDao.mergeUser(user);
				if (cUser != null) {
					DocumentSubType documentSubType = null;
					if (cUser.getEmailId() != null || !cUser.getEmailId().equals("")) {
						documentSubType = payCheckDao.findDocumentSubTypeByName("Email");
						DocumentImage di = new DocumentImage();
						di.setDcumentType(documentSubType.getDocumentType());
						di.setDocumentSubType(documentSubType);
						di.setUser(cUser);
						di.setImageSource(cUser.getEmailId());
						payCheckDao.saveDocumentImage(di);
					}
					if (cUser.getFacebookId() != null || !cUser.getFacebookId().equals("")) {
						documentSubType = payCheckDao.findDocumentSubTypeByName("Facebook");
						DocumentImage di = new DocumentImage();
						di.setDcumentType(documentSubType.getDocumentType());
						di.setDocumentSubType(documentSubType);
						di.setUser(cUser);
						di.setImageSource(cUser.getFacebookId());
						payCheckDao.saveDocumentImage(di);
					}
				}
				String txtmessage = ("Dear " + fuser.getFirstName() + "," + "\n\n Your New OTP is \n" + otp.toString());// New
																														// User's
																														// message
				// IpAddress.sendMail(user, txtmessage, title);// send mail to message
				IpAddress.sendMail(user, otp.toString(), title);

				status.put("status", "true");
				status.put("firstName", user.getFirstName());
				status.put("id", cUser.getId());
				status.put("tokenId", token);

				status.put("verify", "true");
				status.put("message", "Account  verfied");
				/*
				 * if (matchedUser.getKycFrontPic() != null && matchedUser.getKycBackPic() !=
				 * null) { status.put("idProof", "true");// need to check } else {
				 */
				status.put("idProof", "false");
				// }
			}
		}
		return status;

	}

	@RequestMapping(value = "/verify/user/{id}", method = RequestMethod.POST, consumes = { "application/json" })
	public @ResponseBody Map<String, Object> verifyOtp(@PathVariable("id") long id, @RequestBody User user,
			HttpServletRequest request) throws JsonParseException, JsonMappingException, IOException {
		Map<String, Object> map = new HashMap<String, Object>();
		User currentUser = payCheckDao.findUserById(id);
		System.out.println("          verify                ");
		if (currentUser != null) {
			Date startDate = currentUser.getResetPasswords().getDate();// Set start date
			Date endDate = new Date();
			long duration = endDate.getTime() - startDate.getTime();
			long diffInMinutes = TimeUnit.MILLISECONDS.toMinutes(duration);
			if (currentUser.getResetPasswords().getOtp().equals(user.getOtp())) {
				if (diffInMinutes < 10) {
					ResetPassword resetPassword = currentUser.getResetPasswords();
					resetPassword.setVerify(true);
					payCheckDao.updateOtp(resetPassword);
					map.put("status", "true");
					map.put("id", currentUser.getId());
					map.put("firstName", currentUser.getFirstName());
					// map.put("tokenId", currentUser.getTokenId());
				} else {
					try {
						throw new SessionExpiredException("Session Expired");
					} catch (SessionExpiredException se) {
						System.out.println(se.getMessage());
						map.put("status", "false");
						map.put("message", se.getMessage());
					}
				}
			} else {
				map.put("status", "false");
				map.put("message", "OTP not matched");
			}
		} else {
			map.put("status", "false");
			map.put("message", "User Not Found");
		}
		return map;
	}
	/*                                                                                                                    */

	public void sentOtp(User user, StringBuilder numericCode) {
		List<String> deviceList = new ArrayList<String>();
		String userDeviceIdKey = "d1mgGiEi-IQmw9gE:APA91bE4RwZFdc91_o4XT1PIsBhBsYHay_Znuy8RhNfzCeNianUvW2N_pS6WxzqJ96WdbPxnxwQxEAC66dQoyG79uKk_rfP3GzmyTkr4LF89npIRjq9k_8q1JC6IN71jMxud3OV9Xdob";
		// deviceList.add(userDeviceIdKey);
		deviceList.add(user.getDeviceId());
		String message = "Hello " + user.getFirstName() + " Your OTP is " + numericCode;
		// System.out.println(FirebaseInstanceId.getInstance().getToken(););
		org.json.simple.JSONObject messageObject = new org.json.simple.JSONObject();
		messageObject.put("message", message);
		messageObject.put("id", 0);
		messageObject.put("messagetype", NotificationEnum.OTP);
		org.json.simple.JSONObject parentObject = new org.json.simple.JSONObject();
		parentObject.put("data", messageObject);
		parentObject.put("registration_ids", deviceList);

		FireBaseServices.sendFCMNotification(parentObject.toString());

	}

	public boolean emailExist(User email) {
		User user = payCheckDao.findUserByEmail(email);
		if (user != null) {
			return true;
		}
		return false;
	}

	public String createToken(User matchedUser) {
		Key key = MacProvider.generateKey();
		// String compactJws =
		// Jwts.builder().setSubject(matchedUser.getDeviceId()).signWith(SignatureAlgorithm.HS512,
		// key)
		String compactJws = Jwts.builder().setSubject(matchedUser.getEmailId()).signWith(SignatureAlgorithm.HS512, key)
				.compact();
		return compactJws;

	}

	public StringBuilder generateOtp() {

		Random generator = new Random();
		StringBuilder numericCode = new StringBuilder(4);
		while (numericCode.length() < 6) {
			char nextChar = CODE_NUMERICS.charAt(generator.nextInt(CODE_NUMERICS.length()));
			numericCode.append(nextChar);
		}
		return numericCode;
	}

	@RequestMapping(value = "/profilePic/user/{id}", method = RequestMethod.POST)
	public @ResponseBody Map<String, String> saveProfilePics(@PathVariable("id") long id,
			@RequestParam("file") MultipartFile file, HttpServletRequest request,
			@RequestParam("tokenId") String tokenId) throws IOException {
		User currentUser = payCheckDao.findUserById(id);
		Map<String, String> map = new HashMap<String, String>();
		String fileType = FilenameUtils.getExtension(file.getOriginalFilename());
		String folder = "/image/profile_pic/";
		if (currentUser.getTokenId().equals(tokenId)) {
			if (!file.isEmpty()) {
				if (fileType.equals("png") || fileType.equals("jpg")) {
					try {
						saveImage(file, request, folder, currentUser, fileType);
						String contextPath = request.getContextPath();
						// currentUser.setProfilePic(contextPath.concat(folder).concat(currentUser.getEmailId())
						// + "." + fileType);
						currentUser.setProfilePic(StaticIP.IP
								.concat(contextPath.concat(folder).concat(currentUser.getEmailId()) + "." + fileType));
						// docImage.setImageSource(StaticIP.IP.concat(contextPath.concat(folder).concat(currentUser.getEmailId())
						// + "." + fileType));
						payCheckDao.updateUser(currentUser);
						map.put("status", "success");
						map.put("profileImg", currentUser.getProfilePic());
						return map;
					} catch (Exception e) {
						map.put("status", "failure");
						map.put("message", e.getMessage());
					}
				} else {
					map.put("status", "failure");
					map.put("message", "Not valid format");

				}
			} else {
				map.put("status", "failure");
				map.put("message", "Not selected any files");
			}
		} else {
			map.put("status", "failure");
			map.put("message", "session expired");
		}

		return map;
	}

	@RequestMapping(value = "/kycDocument/user/{id}", method = RequestMethod.POST)
	public @ResponseBody Map<String, String> saveDocuments(@PathVariable("id") long id,
			@RequestParam("file") MultipartFile file, HttpServletRequest request,
			@RequestParam("imageSide") String imageSide, @RequestParam("tokenId") String tokenId) throws IOException {
		User currentUser = payCheckDao.findUserById(id);
		Map<String, String> map = new HashMap<String, String>();
		String fileType = FilenameUtils.getExtension(file.getOriginalFilename());
		String folder = "/image/kyc_document/" + imageSide + "/";

		if (currentUser.getTokenId().equals(tokenId)) {
			if (!file.isEmpty()) {
				if (fileType.equals("pdf") || fileType.equals("png") || fileType.equals("jpg")
						|| fileType.equals("docx") || fileType.equals("doc")) {// png doc //jpg
					try {
						saveImage(file, request, folder, currentUser, fileType);
						String contextPath = request.getContextPath();
						if (imageSide.equals("front")) {
							// currentUser.setKycFrontPic(contextPath.concat(folder).concat(currentUser.getEmailId())
							// + "." + fileType);// +"_"+imageSide
							currentUser.setKycFrontPic(StaticIP.IP.concat(
									contextPath.concat(folder).concat(currentUser.getEmailId()) + "." + fileType));// +"_"+imageSide
							map.put("kycDocument", currentUser.getKycFrontPic());
						}
						if (imageSide.equals("back")) {
							// currentUser.setKycBackPic(contextPath.concat(folder).concat(currentUser.getEmailId())
							// + "." + fileType);// +"_"+imageSide+
							currentUser.setKycBackPic(StaticIP.IP.concat(
									contextPath.concat(folder).concat(currentUser.getEmailId()) + "." + fileType));// +"_"+imageSide+
							map.put("kycDocument", currentUser.getKycBackPic());
						}
						payCheckDao.updateUser(currentUser);
						map.put("status", "true");

						return map;
					} catch (Exception e) {
						map.put("status", "false");
						map.put("message", e.getMessage());
					}
				} else {
					map.put("status", "false");
					map.put("message", "Not valid format");

				}
			} else {
				map.put("status", "false");
				map.put("message", "Not selected any files");
			}
		} else {
			map.put("status", "false");
			map.put("message", "session expired");
		}

		return map;
	}

	private void saveImage(MultipartFile file, HttpServletRequest request, String folder, User currentUser,
			String fileType) throws IOException {
		HttpSession session = request.getSession();
		ServletContext sc = session.getServletContext();
		File file1 = new File(sc.getRealPath("/") + folder);
		file1.mkdirs();
		byte[] bytes = file.getBytes();
		BufferedOutputStream stream = new BufferedOutputStream(
				new FileOutputStream(new File(file1 + "/" + currentUser.getEmailId() + "." + fileType)));
		stream.write(bytes);
		stream.close();

	}

	@RequestMapping(value = "/validatePhoneNumber", method = RequestMethod.POST, consumes = { "application/json" })
	public @ResponseBody Map<String, StringBuilder> getValidateMobileNumber(@RequestBody String jsonRecord)
			throws JsonParseException, JsonMappingException, IOException {
		ObjectMapper mapper = new ObjectMapper();
		Map<String, StringBuilder> map = new HashMap<String, StringBuilder>();
		User user = (User) payCheckDao
				.findMobileNumber(mapper.readValue(jsonRecord, SignUpUserBean.class).getPhoneNumber());
		if (user != null) {
			StringBuilder numericCode = generateOtp();

			List<String> deviceList = new ArrayList<String>();
			// deviceList.add(user.getDeviceGcmId());
			String userDeviceIdKey = "d1mgGiEi-UU:APA91bGWztp1DUKD5JvYP7zybyhuBLSOlk_QhpAE6_CAvdZW3y3Dbi7YkuVzRl3c0tnM3jDtwFeHkJr3h_tmT7X3AUq_4JOg2iCSc7E0RRYof71Hu7RpcDYVsys0rw0Mski0_X1_nzUm";
			deviceList.add(userDeviceIdKey);
			String message = "Hello " + user.getFirstName() + " Your OTP is " + numericCode;
			// System.out.println(FirebaseInstanceId.getInstance().getToken(););
			org.json.simple.JSONObject messageObject = new org.json.simple.JSONObject();
			messageObject.put("message", message);
			messageObject.put("id", 0);
			messageObject.put("messagetype", NotificationEnum.OTP);
			org.json.simple.JSONObject parentObject = new org.json.simple.JSONObject();
			parentObject.put("data", messageObject);
			parentObject.put("registration_ids", deviceList);

			FireBaseServices.sendFCMNotification(parentObject.toString());

			map.put("OTP", numericCode);
			return map;

		} else {
			StringBuilder numericCode = new StringBuilder(1);
			map.put("OTP", numericCode.append(Integer.valueOf(0)));
			return map;
		}
	}

	@RequestMapping(value = "/homePage", method = RequestMethod.POST, headers = "Accept=application/json", consumes = {
			"application/json" })
	public @ResponseBody User getHomePage(@RequestBody String jsonRecord)
			throws JsonParseException, JsonMappingException, IOException {
		JSONObject jsonObj = new JSONObject(jsonRecord);
		User user = payCheckDao.findUserById(Long.valueOf((Integer) jsonObj.get("userid")));
		if (user != null) {
			user.setStatus(true);
		} else {
			user.setStatus(false);
		}
		return user;
	}

	@RequestMapping(value = "update/user/{id}", method = RequestMethod.PUT)
	public @ResponseBody Map<String, Object> updateUserDeviceId(@PathVariable("id") long id, @RequestBody User user) {
		System.out.println("Updating User " + id);
		Map<String, Object> map = new HashMap<String, Object>();
		User currentUser = payCheckDao.findUserById(id);
		if (currentUser == null) {
			map.put("status", "false");
		} else {
			currentUser.setFirstName(user.getFirstName());
			currentUser.setLastName(user.getLastName());
			currentUser.setEmailId(user.getEmailId());
			currentUser.setPhoneNumber(user.getPhoneNumber());

			currentUser.setDeviceId(user.getDeviceId());
			payCheckDao.updateUser(currentUser);
			map.put("status", "true");
		}
		return map;
	}

	@RequestMapping(value = "/forgotPassword", method = RequestMethod.POST, consumes = { "application/json" })
	public @ResponseBody Map<String, Object> getForgetPassword(@RequestBody User users, HttpServletRequest request)
			throws JsonParseException, JsonMappingException, IOException {
		// ObjectMapper mapper = new ObjectMapper();
		Map<String, Object> map = new HashMap<String, Object>();

		User currentUser = (User) payCheckDao.findUserByEmail(users);
		if (currentUser != null) {
			StringBuilder otp = generateToken();
			ResetPassword resPass = currentUser.getResetPasswords();
			if (resPass != null) {
				resPass.setOtp(otp.toString());
				resPass.setTime(new Date());
				// resPass.setVerify(false);// setting false to verify on resend OTP
				resPass.setDate(new Date());
				payCheckDao.updateOtp(resPass);
			} else {
				ResetPassword resPassword = new ResetPassword();
				resPassword.setOtp(otp.toString());
				resPassword.setVerify(false);// setting false to verify on resend OTP
				resPassword.setDate(new Date());// (otp.toString());
				resPassword.setUser(currentUser);
				resPassword.setTime(new Date());
				payCheckDao.saveOtp(resPassword);
			}
			String title = "OTP";
			// payCheckDao.updateOtp(resetPassword);
			// IpAddress.sendMail(user, txtmessage, title);
			IpAddress.sendMail(currentUser, otp.toString(), title);
			map.put("status", true);
			map.put("message", "OTP has been sent to mail");

			return map;
		} else {
			map.put("status", false);
			map.put("message", "User Not Found");

		}
		return map;
	}

	public StringBuilder generateToken() {

		Random generator = new Random();
		StringBuilder numericCode = new StringBuilder(4);
		while (numericCode.length() < 6) {
			char nextChar = CODE_ALPHA_NUMERICS.charAt(generator.nextInt(CODE_ALPHA_NUMERICS.length()));
			numericCode.append(nextChar);
		}
		return numericCode;
	}

	/* Create password with user id and OTP */
	@RequestMapping(value = "/updatePassword/user/{id}", method = RequestMethod.POST, consumes = { "application/json" })
	public @ResponseBody Map<String, Object> updatePassword(@PathVariable("id") long id, @RequestBody User user,
			HttpServletRequest request) throws JsonParseException, JsonMappingException, IOException {
		Map<String, Object> map = new HashMap<String, Object>();
		User currentUser = payCheckDao.findUserById(id);
		if (currentUser != null) {
			currentUser.setPassword(user.getPassword());

			if (currentUser.getResetPasswords().getOtp().equals(user.getOtp())) {
				currentUser.setLoogedIn(false);
				payCheckDao.updateUser(currentUser);
				ResetPassword resetPass = currentUser.getResetPasswords();
				resetPass.setVerify(true);
				payCheckDao.updateOtp(resetPass);// updating verify during updating password

				map.put("status", true);
				map.put("message", "Password is updated");
			} else {
				map.put("status", false);
				map.put("message", "OTP not Matched");
			}
		} else {
			map.put("status", false);
			map.put("message", "User not found");

		}

		return map;
	}
	/* changing password with old and new passwords */

	@RequestMapping(value = "/changePassword/user/{id}", method = RequestMethod.POST, consumes = { "application/json" })
	public @ResponseBody Map<String, Object> updatePassword(@PathVariable("id") long id, @RequestBody String jsonRecord,
			HttpServletRequest request) throws JsonParseException, JsonMappingException, IOException {
		JSONObject jsonObj = new JSONObject(jsonRecord);
		String oldPassword = (String) jsonObj.get("oldPassword");
		String newPassword = (String) jsonObj.get("newPassword");

		Map<String, Object> map = new HashMap<String, Object>();
		User currentUser = payCheckDao.findUserById(id);
		if (currentUser != null) {
			if (currentUser.getFacebookId() == null) {
				if (currentUser.getPassword().equals(oldPassword)) {
					currentUser.setPassword(newPassword);
					User user = payCheckDao.mergeUser(currentUser);
					if (user != null) {
						map.put("success", true);
						map.put("message", "Password is updated");
					} else {
						map.put("success", false);
						map.put("message", "Password  not updated");
					}

				} else {
					map.put("success", false);
					map.put("message", "Old password  not matched");
				}
			} else {
				map.put("success", false);
				map.put("message", "You Can not change the passwords");
			}
		} else {
			map.put("success", false);
			map.put("message", "User not found");
		}
		// }
		return map;
	}

	// @RequestMapping(value = "/sendOtp/user/{id}", method = RequestMethod.GET,
	// consumes = { "application/json" })
	@RequestMapping(value = "/resend/user/{id}", method = RequestMethod.GET, consumes = { "application/json" })
	public @ResponseBody Map<String, Object> sendOtpToMail(@PathVariable("id") long id,
			// , @RequestBody String jsonRecord,
			HttpServletRequest request) throws JsonParseException, JsonMappingException, IOException {
		// JSONObject jsonObj = new JSONObject(jsonRecord);
		// String emailId = (String) jsonObj.get("emailId");
		Map<String, Object> map = new HashMap<String, Object>();
		// User currentUser = payCheckDao.findUserByEmail(user);
		/*
		 * StringBuilder token = generateToken(); String title = "OTP"; Map<String,
		 * Object> map = new HashMap<String, Object>(); User user =
		 * payCheckDao.findUserById(id); IpAddress.sendMail(user, token.toString(),
		 * title);
		 */

		// map.put("status", true);
		// map.put("message", "OTP has been sent to mail");

		User currentUser = payCheckDao.findUserById(id);
		if (currentUser != null) {
			StringBuilder otp = generateToken();
			ResetPassword resPass = currentUser.getResetPasswords();
			if (resPass != null) {
				resPass.setOtp(otp.toString());
				resPass.setVerify(false);// setting false to verify on resend OTP
				resPass.setDate(new Date());
				payCheckDao.updateOtp(resPass);
			} else {
				ResetPassword resPassword = new ResetPassword();
				resPassword.setOtp(otp.toString());
				resPassword.setVerify(false);// setting false to verify on resend OTP
				resPassword.setDate(new Date());// (otp.toString());
				resPassword.setUser(currentUser);
				payCheckDao.saveOtp(resPassword);
			}

			String txtmessage = ("Dear " + currentUser.getFirstName() + "," + "\n\n Your New OTP is \n"
					+ otp.toString());
			String title = "OTP";
			// payCheckDao.updateOtp(resPass);
			// IpAddress.sendMail(currentUser, txtmessage, title);
			IpAddress.sendMail(currentUser, otp.toString(), title);
			map.put("status", "true");
			// payCheckDao.updateOtp(resPass);
		} else {
			map.put("status", "false");
		}
		return map;

		// return map;
	}

	@RequestMapping(value = "loan/user/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody Map<String, Object> getUserLoan(@PathVariable("id") long id) {
		List<Loan> listOfLoan = null;
		Map<String, Object> map = new HashMap<String, Object>();
		User user = payCheckDao.findUserById(id);
		if (user != null) {
			System.out.println("User with id " + id + " not found");
			listOfLoan = user.getLoan();
			if (!listOfLoan.isEmpty()) {
				map.put("status", "true");
				map.put("loanList", listOfLoan);
			} else {
				map.put("status", "false");
			}
		} else {
			map.put("status", "false");
		}
		return map;

	}

	@RequestMapping(value = "/user/{id}", method = RequestMethod.PUT)
	public @ResponseBody Map<String, Object> updateUserProfile(@PathVariable("id") long id, @RequestBody User user) {
		System.out.println("Updating User " + id);
		Map<String, Object> map = new HashMap<String, Object>();
		User currentUser = payCheckDao.findUserById(id);

		if (currentUser == null) {
			map.put("status", "false");
		} else {
			if (user.getFirstName() != null && user.getFirstName() != "")
				currentUser.setFirstName(user.getFirstName());
			if (user.getLastName() != null && user.getLastName() != "")
				currentUser.setLastName(user.getLastName());
			if (user.getPhoneNumber() != null && user.getPhoneNumber() != "")
				currentUser.setPhoneNumber(user.getPhoneNumber());
			if (user.getEmailId() != null && user.getEmailId() != "")
				currentUser.setEmailId(user.getEmailId());

			payCheckDao.updateUser(currentUser);
			map.put("status", "true");
			map.put("user", currentUser);
		}
		return map;
	}

	@RequestMapping(value = "/user/{id}", method = RequestMethod.GET)
	public @ResponseBody Map<String, Object> getUser(@PathVariable("id") long id) {
		Map<String, Object> map = new HashMap<String, Object>();
		User currentUser = payCheckDao.findUserById(id);

		map.put("status", "true");
		map.put("user", currentUser);
		return map;
	}

	/*
	 * @RequestMapping(value = "/validateOtp", method = RequestMethod .GET)
	 * public @ResponseBody Map<String, Object> validateOtp() { List<InterestRate>
	 * listOfLoan = payCheckDao.getAllLoan(); Map<String, Object> map = new
	 * HashMap<String, Object>(); map.put("allLoan", listOfLoan); return map;
	 * 
	 * }
	 */

	/*
	 * @RequestMapping(value = "/verify/user", method = RequestMethod.POST, consumes
	 * = { "application/json" }) public @ResponseBody Map<String, Object>
	 * verifyOtpOnSignUp(@RequestBody ResetPassword resetPasswordUser,
	 * HttpServletRequest request) throws JsonParseException, JsonMappingException,
	 * IOException { Map<String, Object> map = new HashMap<String, Object>();
	 * ResetPassword currentUser =
	 * payCheckDao.findResetPasswordByEmail(resetPasswordUser);
	 * System.out.println(currentUser.getDate());
	 * 
	 * Date startDate = currentUser.getDate();// Set start date Date endDate = new
	 * Date();// Set end date long duration = endDate.getTime() -
	 * startDate.getTime(); long diffInMinutes =
	 * TimeUnit.MILLISECONDS.toMinutes(duration);
	 * 
	 * if (currentUser.getOtp().equals(resetPasswordUser.getOtp())) { if
	 * (diffInMinutes < 10) {// checking time diffrence map.put("success", true); }
	 * else { try { throw new SessionExpiredException("Session Expired"); } catch
	 * (SessionExpiredException se) { System.out.println(se.getMessage());
	 * map.put("success", se.getMessage()); } } } else {
	 * 
	 * map.put("success", "OTP Not matched"); }
	 * 
	 * return map; }
	 */

	@RequestMapping(value = "/user/{id}/document", method = RequestMethod.POST) // method should get
	public @ResponseBody Map<String, Object> getBankFinance(@PathVariable("id") long id
	// ,@PathVariable("docId") long docId
	) {
		Map<String, Object> map = new HashMap<String, Object>();
		List<DocumentType> docList = payCheckDao.getAllDocument();
		List<DocumentType> docList1 = new ArrayList<DocumentType>();
		User currentUser = payCheckDao.findUserById(id);
		for (DocumentType docType : docList) {

			List<Object> items = new ArrayList<Object>();
			DocumentType documentTypes = payCheckDao.findDcumentTypeById(docType.getId());
			List<DocumentSubType> listOfSubType = payCheckDao.findDocumentSubTypeByType(documentTypes);

			for (DocumentSubType subType : listOfSubType) {
				BankDetails bankDetails = new BankDetails();
				boolean status = false;
				DocumentImage di = payCheckDao.findDocumentImageByUser(currentUser, documentTypes, subType);
				System.out.println(di);
				if (di != null) {
					if (di.getImageSource() != null && !di.getImageSource().trim().equals("")) {
						status = true;
					}
				}
				bankDetails.setId(subType.getId());
				bankDetails.setName(subType.getName());
				bankDetails.setStatus(status);
				items.add(bankDetails);
			}
			docType.setItems(items);
			docList1.add(docType);
		}
		map.put("root", docList1);
		return map;
	}

	@RequestMapping(value = "/upload/user/{id}", method = RequestMethod.POST)
	public @ResponseBody Map<String, String> uploadDocument(@PathVariable("id") long id,
			@RequestParam("file") MultipartFile file, HttpServletRequest request,
			@RequestParam("documentType") Long documentTypeId, @RequestParam("documentSubType") Long documentSubTypeId,
			@RequestParam("tokenId") String tokenId) throws IOException {

		User currentUser = payCheckDao.findUserById(id);
		Map<String, String> map = new HashMap<String, String>();
		String fileType = FilenameUtils.getExtension(file.getOriginalFilename());
		if (currentUser.getTokenId().equals(tokenId)) {
			if (!file.isEmpty()) {
				if (fileType.equals("pdf") || fileType.equals("png") || fileType.equals("jpg")
				// || fileType.equals("docx") || fileType.equals("doc")
				) {
					String contextPath = request.getContextPath();
					try {
						System.out.println(documentTypeId);
						String folder = null;

						if (documentTypeId != null) {
							if (documentSubTypeId != null) {
								// folder = "/image/paySlip/";
								DocumentType documentTypes = payCheckDao.findDcumentTypeById(documentTypeId);
								DocumentSubType documentSubType = payCheckDao
										.findDocumentSubTypeById(documentSubTypeId);
								folder = "/image/" + documentTypes.getName() + "/" + documentSubType.getLocation()
										+ "/";

								DocumentImage docImage = payCheckDao.findDocumentImageByUser(currentUser, documentTypes,
										documentSubType);
								System.out.println(docImage);
								if (docImage == null) {
									DocumentImage di = new DocumentImage();
									di.setDcumentType(documentTypes);
									di.setDocumentSubType(documentSubType);
									di.setUser(currentUser);
									// di.setImageSource(contextPath.concat(folder).concat(currentUser.getEmailId())
									// + "." + fileType);
									di.setImageSource(StaticIP.IP
											.concat(contextPath.concat(folder).concat(currentUser.getEmailId()) + "."
													+ fileType));
									payCheckDao.saveDocumentImage(di);
									map.put(di.getDocumentSubType().getName(), di.getImageSource());
								} else {
									// docImage.setImageSource(contextPath.concat(folder).concat(currentUser.getEmailId())
									// + "." + fileType);
									docImage.setImageSource(StaticIP.IP
											.concat(contextPath.concat(folder).concat(currentUser.getEmailId()) + "."
													+ fileType));
									payCheckDao.saveDocumentImage(docImage);
									map.put(docImage.getDocumentSubType().getName(), docImage.getImageSource());
								}
							}
						}
						saveImage(file, request, folder, currentUser, fileType);
						map.put("status", "success");
						return map;
					} catch (Exception e) {
						map.put("status", "failure");
						map.put("message", e.getMessage());
					}
				} else {
					map.put("status", "failure");
					map.put("message", "Not valid format");

				}
			} else {
				map.put("status", "failure");
				map.put("message", "Not selected any files");
			}
		} else {
			map.put("status", "failure");
			map.put("message", "session expired");
		}

		return map;
	}
	
	@RequestMapping(value = "/user/{id}/ahead", method = RequestMethod.GET) 
	public @ResponseBody Map<String, Object> getTotalAhead(@PathVariable("id") long id
	) {
		Map<String, Object> map = new HashMap<String, Object>();
		User currentUser = payCheckDao.findUserById(id);
		System.out.println(payCheckDao.findTotalBehindUser(currentUser));
		Long totalBehind  = payCheckDao.findTotalBehindUser(currentUser);
		long totaluser = payCheckDao.findTotalCount();
		System.out.println(totaluser);
		long totalverified = totaluser-totalBehind;
		
		map.put("userIndex", totaluser-totalBehind);
		map.put("totalBehind", totalBehind);
		
		return map;
	
	}
}
