package com.artivatic.paychec.controller;

import java.security.Key;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.artivatic.paychec.bean.SignUpUserBean;
import com.artivatic.paychec.dao.PayCheckDao;
import com.artivatic.paychec.entity.DocumentImage;
import com.artivatic.paychec.entity.DocumentSubType;
import com.artivatic.paychec.entity.ResetPassword;
import com.artivatic.paychec.entity.User;
import com.artivatic.paychec.exception.EmailFoundException;
import com.artivatic.paychec.firebase.IpAddress;
import com.artivatic.paychec.util.GenerateOTP;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.impl.crypto.MacProvider;

@Controller
public class SignupController {
	
	@Autowired
	PayCheckDao payCheckDao;
	
	
	
	
	@RequestMapping(value = "/signUp", method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> signUp(@RequestBody SignUpUserBean signUpUserBean,
			HttpServletRequest request) throws EmailFoundException {
		System.out.println("signup User " + signUpUserBean.getFirstName());
		Map<String, Object> status = new HashMap<String, Object>();
		User user = new User();
		user.setFirstName(signUpUserBean.getFirstName());
		user.setLastName(signUpUserBean.getLastName());
		user.setDeviceId(signUpUserBean.getDeviceId());
		user.setEmailId(signUpUserBean.getEmailId());
		user.setPassword(signUpUserBean.getPassword());
		user.setPhoneNumber(signUpUserBean.getPhoneNumber());
		user.setCreatedDate(new Date());
		String token = createToken(user);
		user.setTokenId(token);

//		StringBuilder otp = generateToken();
		StringBuilder otp = GenerateOTP.generateOtp();
		ResetPassword resetPassword = new ResetPassword();
		resetPassword.setUser(user);
		resetPassword.setTime(new Date());
		resetPassword.setOtp(otp.toString());
		resetPassword.setEmailId(user.getEmailId());
		resetPassword.setDate(new Date());
		resetPassword.setVerify(false);
		user.setResetPasswords(resetPassword);

		User currentUser = payCheckDao.findUserByEmail(user);
		User currentUserP = payCheckDao.findUserByPhoneNumber(user);
//		String txtmessage = ("Dear " + user.getFirstName() + "," + "\n\n Your New OTP is \n" + otp.toString());
		String title = "OTP";
		System.out.println("Email user" + currentUser);
		System.out.println("Phone User" + currentUserP);

		if (currentUser != null) {
			if (!currentUser.getResetPasswords().getVerify()) { // never called
//				ResetPassword rePassword = currentUser.getResetPasswords();
//				rePassword.setOtp(otp.toString());
//				rePassword.setDate(new Date());
//				 resetPassword.setVerify(true);
//				 payCheckDao.updateOtp(rePassword);
//				 IpAddress.sendMail(user, txtmessage, title);
				status.put("status", "false");
				status.put("message", "Email address is already registered. Please verify your account.");
			} else {
				status.put("status", "false");
				status.put("message", "Email " + currentUser.getEmailId() + " already registered.");
			}

		} else if (currentUserP != null) {
			if (!currentUserP.getResetPasswords().getVerify()) {//  never called
//				ResetPassword rePassword = currentUserP.getResetPasswords();
//				rePassword.setOtp(otp.toString());
//				rePassword.setDate(new Date());
				// resetPassword.setVerify(true);
				// payCheckDao.updateOtp(rePassword);

				// IpAddress.sendMail(user, txtmessage, title);
				status.put("status", "false");
				status.put("message", "Mobile no. already registered.Please verify your account. ");
			} else {
				status.put("status", "false");
				status.put("message", "Mobile no. " + currentUserP.getPhoneNumber() + " already registered.");
			}
		} else {
			// Long id = payCheckDao.saveUser(user);
			User cUser = payCheckDao.mergeUser(user);
			if (cUser != null) {
				DocumentSubType documentSubType = null;

				if (cUser.getEmailId() != null || !cUser.getEmailId().equals("")) {// setting document details
					documentSubType = payCheckDao.findDocumentSubTypeByName("Email");
					DocumentImage di = new DocumentImage();
					di.setDcumentType(documentSubType.getDocumentType());
					di.setDocumentSubType(documentSubType);
					di.setUser(cUser);
					di.setImageSource(cUser.getEmailId());
					payCheckDao.saveDocumentImage(di);
				}
			}
//			IpAddress.sendMail(user, txtmessage, title);
			IpAddress.sendMail(user, otp.toString(), title);
			status.put("status", "true");
			status.put("verify", "false");
			status.put("id", cUser.getId());
			status.put("tokenId", token);
		}
		return status;
	}

 
	
	public String createToken(User matchedUser) {
		Key key = MacProvider.generateKey();
		String compactJws = Jwts.builder().setSubject(matchedUser.getEmailId()).signWith(SignatureAlgorithm.HS512, key)
				.compact();
		return compactJws;

	}
	
}
