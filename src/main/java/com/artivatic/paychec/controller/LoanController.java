package com.artivatic.paychec.controller;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

//import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.artivatic.paychec.bean.LoanApplyBean;
import com.artivatic.paychec.bean.PaidLoanBean;
import com.artivatic.paychec.dao.PayCheckDao;
import com.artivatic.paychec.entity.InterestRate;
import com.artivatic.paychec.entity.Loan;
import com.artivatic.paychec.entity.LoanType;
import com.artivatic.paychec.entity.PaidLoan;
import com.artivatic.paychec.entity.User;
import com.artivatic.paychec.entity.UserBankAccount;

@Controller
public class LoanController {
	
	
//	private static final Logger logger = Logger.getLogger(LoanController.class);
	
	@Autowired
	PayCheckDao payCheckDao;
	/* this api is use to pay amount by user*/
	@RequestMapping(value = "loan/pay/user/{id}", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody Map<String, Object> payToLoan(@PathVariable("id") long id,@RequestBody PaidLoanBean paidLoanBean) {
		Map<String, Object> map = new HashMap<String, Object>();
		User user = payCheckDao.findUserById(id);
		if (user != null) {
			PaidLoan paidLoan = new PaidLoan(); 
			if(paidLoanBean.getPaidAmount() != null) {
				paidLoan.setPaidAmount(paidLoanBean.getPaidAmount());
			}

			Loan loan = payCheckDao.findLoanById(Long.valueOf(paidLoanBean.getLoanId()));
			paidLoan.setLoan(loan);
			
			paidLoan.setPaidDate(new Date());
			paidLoan.setUser(user);
			Long loanId  = payCheckDao.savePaidLoan(paidLoan);
			if(loanId!= null) {
				map.put("status", true);
				map.put("message", "Your Amount has been paid. ");
			}else {
				map.put("status", "false");
				map.put("message", "Your Amount has been not paid. ");
			}
		 
		} else {
			map.put("status", "false");
			map.put("message", "User not exist. ");
		}
		return map;

	}
	
	/* api is use for user to get their list of loan with their paid or unpaid amount*/
//	@RequestMapping(value = "/loanDetail/user/{id}", method = RequestMethod.GET)
	@RequestMapping(value = "/user/{id}/loanDetail", method = RequestMethod.GET)
	public @ResponseBody Map<String, Object> getloanDetail(@PathVariable("id") long id) {
		Map<String, Object> map = new HashMap<String, Object>();
		User user = payCheckDao.findUserById(id);
		if (user != null) {

			List<Loan> loanList = user.getLoan();
//			BigDecimal amount  = BigDecimal.ZERO;
			for (Loan loan : loanList) {
				List<PaidLoan> paidList = loan.getPaidLoan();
				BigDecimal amount  = BigDecimal.ZERO;
				for (PaidLoan paid : paidList) {
					amount = amount.add(paid.getPaidAmount());
				}
				System.out.println("Amount    "+amount);
				loan.setPaidAmount(amount);
				
				loan.setUnPaidAmount(loan.getLoanAmount().subtract(amount));

			}
			map.put("status", true);
			map.put("loanList", loanList);
		} else {
			map.put("status", false);
			map.put("message", "user not found");
		}

		return map;
	}
	
	@RequestMapping(value = "/getAllLoan", method = RequestMethod.GET)
	public @ResponseBody Map<String, Object> getAllLoan() {
		List<LoanType> listOfLoan = payCheckDao.getLoanType();

		Map<String, Object> map = new HashMap<String, Object>();
		map.put("allLoan", listOfLoan);
		return map;

	}
	
	
	@RequestMapping(value = "/user/{id}/getloanDetail", method = RequestMethod.GET)
	public @ResponseBody Map<String, Object> getAllLoanwithAppliedLoan(@PathVariable("id") long id) {
		// List<InterestRate> listOfLoan = payCheckDao.getAllLoan();
		List<LoanType> listOfLoan = payCheckDao.getLoanType();

		Map<String, Object> map = new HashMap<String, Object>();
//		Map<String, Object> map = new HashMap<String, Object>();
		User user = payCheckDao.findUserById(id);
		map.put("appliedLoan", user.getLoan());
		map.put("allLoan", listOfLoan);
		return map;

	}
	@RequestMapping(value = "/user/{id}/loanapply", method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> loanApply(@PathVariable("id") long id,
			@RequestBody LoanApplyBean loanApplyBean) throws ParseException {
		System.out.println("*********************");
		Map<String, Object> map = new HashMap<String, Object>();
		User user = payCheckDao.findUserById(id);
		if (user != null) {

			Loan loan = new Loan();
			loan.setLoanAmount(loanApplyBean.getLoanAmount());
//			loan.setLoanTenure(loanApplyBean.getLoanTenure());
//			loan.setLoanProceesingAmount(loanApplyBean.getLoanProceesingAmount());
			// loan.setLoanInterestRate(loanApplyBean.getLoanInterestRate());
			String rateId = loanApplyBean.getRateId();

			InterestRate rate = payCheckDao.findRateById(Long.valueOf(rateId));
			System.out.println(rate.getRate());
			loan.setInterestRate(rate);
			System.out.println(loanApplyBean.getLoanCreatedDate());
			SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
			Date createdDate = formatter.parse(loanApplyBean.getLoanCreatedDate());
			System.out.println(createdDate);
			loan.setLoanCreatedDate(createdDate);

			// List<UserBankAccount> userBankList = new ArrayList<UserBankAccount>();
			UserBankAccount bankAccount = new UserBankAccount();
			bankAccount.setBankAccountNumber(loanApplyBean.getAccountNumber());

			bankAccount.setUser(user);
			loan.setUserBankAccount(bankAccount);

			loan.setUser(user);
			try {
				Long loanId = payCheckDao.saveLoan(loan);
				// Loan loanId = payCheckDao.saveLoan(loan);
				System.out.println("********************");

				map.put("status", true);
				map.put("loanId", loanId);

			} catch (Exception e) {
				map.put("status", false);
				map.put("message", e.getMessage());
			}
		} else {
			map.put("status", false);
			map.put("message", "user not found");
		}
		return map;

	}
	@RequestMapping(value = "/user/{id}/loanCount", method = RequestMethod.GET)// not in use
	public @ResponseBody Map<String, Object> getloanCount(@PathVariable("id") long id) {
		Map<String, Object> map = new HashMap<String, Object>();
//		 logger.info("This is an info log entry");
		User user = payCheckDao.findUserById(id);
		List<Loan> loan  = new ArrayList<Loan>();
		boolean status = false;
		if (user != null) {
			loan  = user.getLoan();
			map.put("status", true);
//			map.put("loanList", loanList);
			status = true;
			int fakeData = 2000;
			System.out.println("size   "+loan.size());
			if(loan.size() >0) {
			map.put("total", fakeData + loan.size());
			}else {
				map.put("total",loan.size());
			}
		}
		else {
			map.put("status", false);
			map.put("total",loan.size());
		}
		
//		map.put("total", user.getLoan().size());
		return map;
		}

	@RequestMapping(value = "/user/{id}/loanDetailss", method = RequestMethod.GET)// not in use
	public @ResponseBody Map<String, Object> getloanDetails(@PathVariable("id") long id) {
		Map<String, Object> map = new HashMap<String, Object>();
		User user = payCheckDao.findUserById(id);
//		logger.debug(user);
//		 logger.info("This is an info log entry");
		if (user != null) {
//			List<Loan> loan = new ArrayList<Loan>();

			List<Loan> loanList = user.getLoan();
			 
			map.put("status", true);
			map.put("loanList", loanList);
		} else {
			map.put("status", false);
			map.put("message", "user not found");
		}

		return map;
	}

}
