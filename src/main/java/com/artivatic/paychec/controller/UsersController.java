package com.artivatic.paychec.controller;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.management.ManagementFactory;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.security.Key;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.management.MBeanServer;
import javax.management.MalformedObjectNameException;
import javax.management.ObjectName;
import javax.management.Query;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.portlet.ModelAndView;
import org.springframework.web.util.UriComponentsBuilder;

import com.artivatic.paychec.bean.BankDetails;
import com.artivatic.paychec.bean.FacebookLoginUser;
import com.artivatic.paychec.bean.SignUpUserBean;
import com.artivatic.paychec.dao.PayCheckDao;
import com.artivatic.paychec.document.DocumentTypes;
import com.artivatic.paychec.entity.BankFinanceDetails;
import com.artivatic.paychec.entity.DocumentType;
import com.artivatic.paychec.entity.DocumentImage;
import com.artivatic.paychec.entity.DocumentSubType;
import com.artivatic.paychec.entity.KycDocumentDetails;
import com.artivatic.paychec.entity.Loan;
import com.artivatic.paychec.entity.LoanType;
import com.artivatic.paychec.entity.User;
import com.artivatic.paychec.firebase.IpAddress;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.SignatureException;
import io.jsonwebtoken.impl.crypto.MacProvider;


@RestController
public class UsersController {
	
	 @Autowired 
	 PayCheckDao payCheckDao;
	
	
	 /*@RequestMapping(value = "/user/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	    public ResponseEntity<User> getUser(@PathVariable("id") long id) {
	        User user = payCheckDao.findUserById(id);
	        if (user == null) {
	            System.out.println("User with id " + id + " not found");
	            return new ResponseEntity<User>(HttpStatus.NOT_FOUND);
	        }
	        return new ResponseEntity<User>(user, HttpStatus.OK);
	    }*/
	 
	 @RequestMapping(value = "/home", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	    public Map<String, String> getWelcome() {
		 
		 Map<String, String> map = new HashMap<String, String>();
		 map.put("message"," Hello Paychec");
		 return map;


	    }
	 
	 
	 
	 @RequestMapping(value = "/fblogins", method = RequestMethod.POST)
		public @ResponseBody Map<String, Object> createUsers(@RequestBody FacebookLoginUser fuser,
				HttpServletRequest request
				 ) throws ServletException,SignatureException {
		 String tokenId = "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJyYXVzaGFucmFuamFuODVAZ21haWwuY29tIn0.aKy1UqC8L7_SiyjaVlRBsRqLW_9mbqIpEzd6QN1oalCsE_Uq4QLoX2O-8b51XGkCTMJljI7jaNzaWAKLVuFzQg";
		 
		 
			
			final String authHeader = request.getHeader("authorization");
			System.out.println(authHeader);
			
			 
	        final String token = authHeader.substring(7);
	        System.out.println(token);

//	          Claims claims = Jwts.parser().setSigningKey("secretkey").parseClaimsJws(token).getBody();
	        try {
	            final Claims claims = Jwts.parser().setSigningKey("secretkey").parseClaimsJws(tokenId).getBody();
//	            request.setAttribute("claims", claims);
//	            final Claims claimss =  Jwts.parser().setSigningKey("secretkey").parse(token).getBody();
	        } catch (final SignatureException e) {
	            throw new ServletException("Invalid token");
	        }
//	          System.out.println(claims);
 
			return null;

		}

	 
	 @RequestMapping(value = "/uploadDocuments/user/{id}", method = RequestMethod.POST)
		public @ResponseBody Map<String, String> uploadBankFinanceDetails(@PathVariable("id") long id,
				@RequestParam("file") MultipartFile file, HttpServletRequest request,@RequestParam("documentType") String documentType,
				@RequestParam("tokenId") String tokenId) throws IOException {
			
			System.out.println("^^^^^^^^^^^^^^^^^^");
			User currentUser = payCheckDao.findUserById(id);
			Map<String, String> map = new HashMap<String, String>();
			String fileType = FilenameUtils.getExtension(file.getOriginalFilename());
			if (currentUser.getTokenId().equals(tokenId)) {
				if (!file.isEmpty()) {
					if (fileType.equals("pdf") || fileType.equals("png") || fileType.equals("jpg")
//							|| fileType.equals("docx") || fileType.equals("doc")
							) {
						String contextPath = request.getContextPath();
						try {
							System.out.println(documentType);
							String folder = null;
							BankFinanceDetails bfd = new BankFinanceDetails();
							
							if(DocumentTypes.INSURENCE .equals(documentType) ) {
								System.out.println("************************");
								 folder = "/image/insurence/";
								 
								 if(currentUser.getBankFinanceDetails() != null) {
									 currentUser.getBankFinanceDetails().setInsurence(contextPath.concat(folder).concat(currentUser.getEmailId()) + "." + fileType);
									 payCheckDao.updateBankFinanceDetails(currentUser.getBankFinanceDetails());
									 map.put("insurence", currentUser.getBankFinanceDetails().getInsurence());
								 }else {
								 
								 bfd.setInsurence( contextPath.concat(folder).concat(currentUser.getEmailId()) + "." + fileType);
								 bfd.setUser(currentUser);
								 payCheckDao.saveBankFinanceDetails(bfd);
								 map.put("insurence", bfd.getInsurence());
								 }
							}
							
							if(DocumentTypes.PAYSLIP .equals(documentType) ) {
								System.out.println("************************"+documentType);
								 folder = "/image/pay_slip/";
								 
								 if(currentUser.getBankFinanceDetails() != null) {
									 currentUser.getBankFinanceDetails().setPaySlip( contextPath.concat(folder).concat(currentUser.getEmailId()) + "." + fileType);
									 payCheckDao.updateBankFinanceDetails(currentUser.getBankFinanceDetails());
									 map.put("paySlip", currentUser.getBankFinanceDetails().getPaySlip());
								 }else {
								 
								 bfd.setPaySlip( contextPath.concat(folder).concat(currentUser.getEmailId()) + "." + fileType);
								 bfd.setUser(currentUser);
								 payCheckDao.saveBankFinanceDetails(bfd);
								 map.put("paySlip", bfd.getPaySlip());
								 }
							}
							
							
							if(DocumentTypes.BANKSTATEMENT .equals(documentType) ) {
								System.out.println("************************");
								 folder = "/image/bank_statement/";
								 
								 if(currentUser.getBankFinanceDetails() != null) {
									 currentUser.getBankFinanceDetails().setBankStatement( contextPath.concat(folder).concat(currentUser.getEmailId()) + "." + fileType);
									 payCheckDao.updateBankFinanceDetails(currentUser.getBankFinanceDetails());
									 map.put("bankstatement", currentUser.getBankFinanceDetails().getBankStatement());
								 }else {
								 
								 bfd.setBankStatement( contextPath.concat(folder).concat(currentUser.getEmailId()) + "." + fileType);
								 bfd.setUser(currentUser);
								 payCheckDao.saveBankFinanceDetails(bfd);
								 map.put("bankstatement", bfd.getBankStatement());
								 }
							}
							
							if(DocumentTypes.CREDITCARD .equals(documentType) ) {
								System.out.println("************************");
								 folder = "/image/credit_card/";
								 
								 if(currentUser.getBankFinanceDetails() != null) {
									 currentUser.getBankFinanceDetails().setCreditCard( contextPath.concat(folder).concat(currentUser.getEmailId()) + "." + fileType);
									 payCheckDao.updateBankFinanceDetails(currentUser.getBankFinanceDetails());
									 map.put("creditcard", currentUser.getBankFinanceDetails().getCreditCard());
								 }else {
								 
								 bfd.setCreditCard( contextPath.concat(folder).concat(currentUser.getEmailId()) + "." + fileType);
								 bfd.setUser(currentUser);
								 payCheckDao.saveBankFinanceDetails(bfd);
								 map.put("creditcard", bfd.getCreditCard());
								 }
							}
							saveImage(file, request, folder, currentUser, fileType);
							map.put("status", "success");
							return map;
						} catch (Exception e) {
							map.put("status", "failure");
							map.put("message", e.getMessage());
						}
					} else {
						map.put("status", "failure");
						map.put("message", "Not valid format");

					}
				} else {
					map.put("status", "failure");
					map.put("message", "Not selected any files");
				}
			} else {
				map.put("status", "failure");
				map.put("message", "session expired");
			}

			return map;
		}
	 
	 
	 
	 @RequestMapping(value = "/uploadKycDocuments/user/{id}", method = RequestMethod.POST)
		public @ResponseBody Map<String, String> uploadKycDocumentDetails(@PathVariable("id") long id,
				@RequestParam("file") MultipartFile file, HttpServletRequest request,@RequestParam("documentType") String documentType,
				@RequestParam("tokenId") String tokenId) throws IOException {
			
			System.out.println("^^^^^^^^^^^^^^^^^^");
			User currentUser = payCheckDao.findUserById(id);
			Map<String, String> map = new HashMap<String, String>();
			String fileType = FilenameUtils.getExtension(file.getOriginalFilename());
			if (currentUser.getTokenId().equals(tokenId)) {
				if (!file.isEmpty()) {
					if (fileType.equals("pdf") || fileType.equals("png") || fileType.equals("jpg")
//							|| fileType.equals("docx") || fileType.equals("doc")
							) {
						String contextPath = request.getContextPath();
						try {
							System.out.println(documentType);
							String folder = null;
							KycDocumentDetails kycDocumentDetails = new KycDocumentDetails();
							
							if(DocumentTypes.PASSPORT .equals(documentType) ) {
								System.out.println("************************");
								 folder = "/image/passport/";
								 
								 if(currentUser.getKycDocumentDetails() != null) {
									 currentUser.getKycDocumentDetails().setPassport(contextPath.concat(folder).concat(currentUser.getEmailId()) + "." + fileType);
									 payCheckDao.updateKycDocumentDetails(currentUser.getKycDocumentDetails());
									 map.put("passport", currentUser.getKycDocumentDetails().getPassport());
								 }else {
								 kycDocumentDetails.setPassport(contextPath.concat(folder).concat(currentUser.getEmailId()) + "." + fileType);
								 kycDocumentDetails.setUser(currentUser);
								 payCheckDao.saveKycDocumentDetails(kycDocumentDetails);
								 map.put("passport", kycDocumentDetails.getPassport());
								 }
							}
							
							if(DocumentTypes.PHOTO .equals(documentType) ) {
								System.out.println("************************"+documentType);
								 folder = "/image/photo/";
								 if(currentUser.getKycDocumentDetails() != null) {
									 currentUser.getKycDocumentDetails().setPhoto(contextPath.concat(folder).concat(currentUser.getEmailId()) + "." + fileType);
									 payCheckDao.updateBankFinanceDetails(currentUser.getBankFinanceDetails());
									 map.put("photo", currentUser.getKycDocumentDetails().getPhoto());
								 }else {
								 
									 kycDocumentDetails.setPhoto(contextPath.concat(folder).concat(currentUser.getEmailId()) + "." + fileType);
									 kycDocumentDetails.setUser(currentUser);
								 payCheckDao.saveKycDocumentDetails(kycDocumentDetails);
								 map.put("photo", kycDocumentDetails.getPhoto());
								 }
							}
							
							if(DocumentTypes.HEALTHCERTIFICATE .equals(documentType) ) {
								System.out.println("************************"+documentType);
								 folder = "/image/health_certificate/";
								 if(currentUser.getKycDocumentDetails() != null) {
									 currentUser.getKycDocumentDetails().setHealthcertificate(contextPath.concat(folder).concat(currentUser.getEmailId()) + "." + fileType);
									 payCheckDao.updateBankFinanceDetails(currentUser.getBankFinanceDetails());
									 map.put("health", currentUser.getKycDocumentDetails().getHealthcertificate());
								 }else {
								 
									 kycDocumentDetails.setHealthcertificate(contextPath.concat(folder).concat(currentUser.getEmailId()) + "." + fileType);
									 kycDocumentDetails.setUser(currentUser);
								 payCheckDao.saveKycDocumentDetails(kycDocumentDetails);
								 map.put("health", kycDocumentDetails.getHealthcertificate());
								 }
							}
							
							
						 
							saveImage(file, request, folder, currentUser, fileType);
							map.put("status", "success");
							return map;
						} catch (Exception e) {
							map.put("status", "failure");
							map.put("message", e.getMessage());
						}
					} else {
						map.put("status", "failure");
						map.put("message", "Not valid format");

					}
				} else {
					map.put("status", "failure");
					map.put("message", "Not selected any files");
				}
			} else {
				map.put("status", "failure");
				map.put("message", "session expired");
			}

			return map;
		}
		
		
		private void saveImage(MultipartFile file, HttpServletRequest request, String folder, User currentUser,
				String fileType) throws IOException {
			HttpSession session = request.getSession();
			ServletContext sc = session.getServletContext();
			File file1 = new File(sc.getRealPath("/") + folder);
			file1.mkdirs();
			byte[] bytes = file.getBytes();
			BufferedOutputStream stream = new BufferedOutputStream(
					new FileOutputStream(new File(file1 + "/" + currentUser.getEmailId() + "." + fileType)));
			stream.write(bytes);
			stream.close();
		}
		
		
		/*@RequestMapping(value = "/user/{id}/document/{docId}", method = RequestMethod.POST)
		public @ResponseBody Map<String, Object> getBankFinance(@PathVariable("id") long id,@PathVariable("docId") long docId ) {
			Map<String, Object> map = new HashMap<String, Object>();
			
			User currentUser = payCheckDao.findUserById(id);
			DocumentType documentTypes = payCheckDao.findDcumentTypeById(docId);
			List<DocumentSubType> listOfSubType = payCheckDao.findDocumentSubTypeByType(documentTypes);
			Set<BankDetails> set = new HashSet<BankDetails>();
			for(DocumentSubType subType : listOfSubType) {
				BankDetails bankDetails = new BankDetails();
				boolean status = false;
				DocumentImage di = payCheckDao.findDocumentImageByUser(currentUser, documentTypes, subType);
				System.out.println(di);
				if(di != null) {
				if( di.getImageSource() != null && !di.getImageSource().trim().equals("")  ) {
					status = true;
				}
				}

				bankDetails.setId(subType.getId());
				bankDetails.setName(subType.getName());
				bankDetails.setStatus(status);
				set.add(bankDetails);
			}
			map.put("bankDetails", set);
			return map;
		}*/
/*		
		@RequestMapping(value = "/user/{id}/document", method = RequestMethod.POST)
		public @ResponseBody Map<String, Object> getBankFinance(@PathVariable("id") long id
//				,@PathVariable("docId") long docId
				) {
			Map<String, Object> map = new HashMap<String, Object>();
			List<DocumentType> docList  = payCheckDao.getAllDocument();
			Map<String, Object> map1 = new HashMap<String, Object>();
			
			
			User currentUser = payCheckDao.findUserById(id);
			for(DocumentType  docType : docList) {
				
				Collection  list = new ArrayList ();
//				Set<BankDetails> set = new HashSet<BankDetails>();
				Set<BankDetails> set = new HashSet<BankDetails>();
			DocumentType documentTypes = payCheckDao.findDcumentTypeById(docType.getId());
			List<DocumentSubType> listOfSubType = payCheckDao.findDocumentSubTypeByType(documentTypes);
			
			for(DocumentSubType subType : listOfSubType) {
				BankDetails bankDetails = new BankDetails();
				boolean status = false;
				DocumentImage di = payCheckDao.findDocumentImageByUser(currentUser, documentTypes, subType);
				System.out.println(di);
				if(di != null) {
				if( di.getImageSource() != null && !di.getImageSource().trim().equals("")  ) {
					status = true;
				}
				}

				bankDetails.setId(subType.getId());
				bankDetails.setName(subType.getName());
				bankDetails.setStatus(status);
				set.add(bankDetails);
				
				
				
			}
			list.add(docType);
			list.add(set);
//			map.put("id", docType.getId());
			map.put(docType.getName(), set);
			
			}
			
			
			
			
			return map;
		}
		*/
		
		
	/*	@RequestMapping(value = "/user/{id}/document", method = RequestMethod.POST)// method should get
		public @ResponseBody Map<String, Object> getBankFinance(@PathVariable("id") long id
//				,@PathVariable("docId") long docId
				) {
			Map<String, Object> map = new HashMap<String, Object>();
			List<DocumentType> docList  = payCheckDao.getAllDocument();
			Map<String, Object> map1 = new HashMap<String, Object>();
			List<DocumentType> docList1  = new ArrayList<DocumentType>();
			
			
			User currentUser = payCheckDao.findUserById(id);
			for(DocumentType  docType : docList) {
				
				List<Object>  items = new ArrayList<Object> ();
//				Set<BankDetails> set = new HashSet<BankDetails>();
				Set<BankDetails> set = new HashSet<BankDetails>();
				List<BankDetails> listOfBankDetails = new ArrayList<BankDetails>();
			DocumentType documentTypes = payCheckDao.findDcumentTypeById(docType.getId());
			List<DocumentSubType> listOfSubType = payCheckDao.findDocumentSubTypeByType(documentTypes);
			
			for(DocumentSubType subType : listOfSubType) {
				BankDetails bankDetails = new BankDetails();
				boolean status = false;
				DocumentImage di = payCheckDao.findDocumentImageByUser(currentUser, documentTypes, subType);
				System.out.println(di);
				if(di != null) {
				if( di.getImageSource() != null && !di.getImageSource().trim().equals("")  ) {
					status = true;
				}
				}

				bankDetails.setId(subType.getId());
				bankDetails.setName(subType.getName());
				bankDetails.setStatus(status);
//				set.add(bankDetails);
				items.add(bankDetails);
				
				
				
			}
//			docType.setDocumentList(listOfBankDetails);
//			list.add(docType);
//			list.add(set);
			docType.setItems(items);
//			map.put(docType.getName(), docType);
			docList1.add(docType);
			
//			map.put("id", docType.getId());
//			map.put("name",docType.getName());
//			map.put("items", list);
			
			}
			map.put("root", docList1);
			
			
			
			
			return map;
		}
		
		
		
		 @RequestMapping(value = "/upload/user/{id}", method = RequestMethod.POST)
			public @ResponseBody Map<String, String> uploadDocument(@PathVariable("id") long id,
					@RequestParam("file") MultipartFile file, HttpServletRequest request,
					@RequestParam("documentType") Long documentTypeId,
					@RequestParam("documentSubType") Long documentSubTypeId,
					@RequestParam("tokenId") String tokenId) throws IOException {
				
				System.out.println("^^^^^^^^^^^^^^^^^^");
				User currentUser = payCheckDao.findUserById(id);
				Map<String, String> map = new HashMap<String, String>();
				String fileType = FilenameUtils.getExtension(file.getOriginalFilename());
				if (currentUser.getTokenId().equals(tokenId)) {
					if (!file.isEmpty()) {
						if (fileType.equals("pdf") || fileType.equals("png") || fileType.equals("jpg")
//								|| fileType.equals("docx") || fileType.equals("doc")
								) {
							String contextPath = request.getContextPath();
							try {
								System.out.println(documentTypeId);
								String folder = null;
//								KycDocumentDetails kycDocumentDetails = new KycDocumentDetails();
//								if(documentSubType )
								if(documentTypeId != null) {
									if(documentSubTypeId != null) {
//										 folder = "/image/paySlip/";
										DocumentType documentTypes = payCheckDao.findDcumentTypeById(documentTypeId);
										DocumentSubType documentSubType = payCheckDao.findDocumentSubTypeById(documentSubTypeId);
										 folder = "/image/"+documentTypes.getName()+"/"+documentSubType.getLocation()+"/";
										
										
										DocumentImage docImage = 	payCheckDao.findDocumentImageByUser(currentUser, documentTypes, documentSubType);
										System.out.println(docImage);
										if(docImage == null) {
											DocumentImage di = new DocumentImage();
											di.setDcumentType(documentTypes);
											di.setDocumentSubType(documentSubType);
											di.setUser(currentUser);
											di.setImageSource(contextPath.concat(folder).concat(currentUser.getEmailId()) + "." + fileType);
											payCheckDao.saveDocumentImage(di);
										    map.put(di.getDocumentSubType().getName(), di.getImageSource());
										}else {
											
											docImage.setImageSource(contextPath.concat(folder).concat(currentUser.getEmailId()) + "." + fileType);
											payCheckDao.saveDocumentImage(docImage);
											map.put(docImage.getDocumentSubType().getName(), docImage.getImageSource());
											
										}
										
										
									}
								}
								
							 
								saveImage(file, request, folder, currentUser, fileType);
								map.put("status", "success");
								return map;
							} catch (Exception e) {
								map.put("status", "failure");
								map.put("message", e.getMessage());
							}
						} else {
							map.put("status", "failure");
							map.put("message", "Not valid format");

						}
					} else {
						map.put("status", "failure");
						map.put("message", "Not selected any files");
					}
				} else {
					map.put("status", "failure");
					map.put("message", "session expired");
				}

				return map;
			}*/
		
	
		/*@RequestMapping(value = "/payslip/user/{id}", method = RequestMethod.POST)
		public @ResponseBody Map<String, String> uploadPayslip(@PathVariable("id") long id,
				@RequestParam("file") MultipartFile file, HttpServletRequest request,
				@RequestParam("tokenId") String tokenId) throws IOException {
			User currentUser = payCheckDao.findUserById(id);
			Map<String, String> map = new HashMap<String, String>();
			String fileType = FilenameUtils.getExtension(file.getOriginalFilename());
			String folder = "/image/pay_slip/";
			if (currentUser.getTokenId().equals(tokenId)) {
				if (!file.isEmpty()) {
					if (fileType.equals("pdf") || fileType.equals("png") || fileType.equals("jpg")
							|| fileType.equals("docx") || fileType.equals("doc")) {// png doc //jpg
						try {
							saveImage(file, request, folder, currentUser, fileType);
							String contextPath = request.getContextPath();
							currentUser.setPayslipDocument(
									contextPath.concat(folder).concat(currentUser.getEmailId()) + "." + fileType);
							payCheckDao.updateUser(currentUser);
							map.put("status", "success");
							map.put("payslipDocument", currentUser.getPayslipDocument());
							return map;
						} catch (Exception e) {
							map.put("status", "failure");
							map.put("message", e.getMessage());
						}
					} else {
						map.put("status", "failure");
						map.put("message", "Not valid format");

					}
				} else {
					map.put("status", "failure");
					map.put("message", "Not selected any files");
				}
			} else {
				map.put("status", "failure");
				map.put("message", "session expired");
			}

			return map;
		}*/

		/*@RequestMapping(value = "/insurence/user/{id}", method = RequestMethod.POST)
		public @ResponseBody Map<String, String> uploadInsuranceDetails(@PathVariable("id") long id,
				@RequestParam("file") MultipartFile file, HttpServletRequest request,
				@RequestParam("tokenId") String tokenId) throws IOException {
			User currentUser = payCheckDao.findUserById(id);
			Map<String, String> map = new HashMap<String, String>();
			String fileType = FilenameUtils.getExtension(file.getOriginalFilename());
			String folder = "/image/insurence_document/";
			if (currentUser.getTokenId().equals(tokenId)) {
				if (!file.isEmpty()) {
					if (fileType.equals("pdf") || fileType.equals("png") || fileType.equals("jpg")
							|| fileType.equals("docx") || fileType.equals("doc")) {// png doc //jpg
						try {
							saveImage(file, request, folder, currentUser, fileType);
							String contextPath = request.getContextPath();
							currentUser.setInsurenceDocument(
									contextPath.concat(folder).concat(currentUser.getEmailId()) + "." + fileType);
							payCheckDao.updateUser(currentUser);
							map.put("status", "success");
							map.put("insurenceDocument", currentUser.getInsurenceDocument());
							return map;
						} catch (Exception e) {
							map.put("status", "failure");
							map.put("message", e.getMessage());
						}
					} else {
						map.put("status", "failure");
						map.put("message", "Not valid format");

					}
				} else {
					map.put("status", "failure");
					map.put("message", "Not selected any files");
				}
			} else {
				map.put("status", "failure");
				map.put("message", "session expired");
			}

			return map;
		}*/

		/*@RequestMapping(value = "/uploadDocument/user/{id}", method = RequestMethod.POST)
		public @ResponseBody Map<String, String> uploadPassport(@PathVariable("id") long id,
				@RequestParam("file") MultipartFile file, HttpServletRequest request,
				@RequestParam("documentType") String documentType, @RequestParam("tokenId") String tokenId)
				throws IOException {

			System.out.println("^^^^^^^^^^^^^^^^^^");
			User currentUser = payCheckDao.findUserById(id);
			Map<String, String> map = new HashMap<String, String>();
			String fileType = FilenameUtils.getExtension(file.getOriginalFilename());
			if (currentUser.getTokenId().equals(tokenId)) {
				if (!file.isEmpty()) {
					if (fileType.equals("pdf") || fileType.equals("png") || fileType.equals("jpg")
					// || fileType.equals("docx") || fileType.equals("doc")
					) {
						String contextPath = request.getContextPath();
						try {
							String folder = null;
							if (DocumentTypes.PASSPORT.equals(documentType)) {
								folder = "/image/passport/";
								currentUser.setPassport(
										contextPath.concat(folder).concat(currentUser.getEmailId()) + "." + fileType);
								map.put("passport", currentUser.getPassport());
							}
							if (DocumentTypes.BANKSTATEMENT.equals(documentType)) {
								folder = "/image/bankstatement/";
								currentUser.setBankstatement(
										contextPath.concat(folder).concat(currentUser.getEmailId()) + "." + fileType);
								map.put("bankstatement", currentUser.getBankstatement());
							}

							if (DocumentTypes.HEALTHCERTIFICATE.equals(documentType)) {
								folder = "/image/health_certificate/";
								currentUser.setHelathCertificate(
										contextPath.concat(folder).concat(currentUser.getEmailId()) + "." + fileType);
								map.put("bankstatement", currentUser.getHelathCertificate());
							}

							if (DocumentTypes.CREDITCARD.equals(documentType)) {
								folder = "/image/credit_card/";
								currentUser.setCreditCard(
										contextPath.concat(folder).concat(currentUser.getEmailId()) + "." + fileType);
								map.put("bankstatement", currentUser.getCreditCard());
							}

							saveImage(file, request, folder, currentUser, fileType);

							payCheckDao.updateUser(currentUser);
							map.put("status", "success");

							return map;
						} catch (Exception e) {
							map.put("status", "failure");
							map.put("message", e.getMessage());
						}
					} else {
						map.put("status", "failure");
						map.put("message", "Not valid format");

					}
				} else {
					map.put("status", "failure");
					map.put("message", "Not selected any files");
				}
			} else {
				map.put("status", "failure");
				map.put("message", "session expired");
			}

			return map;
		}
	*/

		/*
		 * @RequestMapping(value = "/kycDocument/user/{id}", method =
		 * RequestMethod.POST) public @ResponseBody Map<String, String>
		 * saveDocuments(@PathVariable("id") long id,
		 * 
		 * @RequestParam("file") MultipartFile file, HttpServletRequest
		 * request,@RequestParam("imageSide") String imageSide,
		 * 
		 * @RequestParam("tokenId") String tokenId) throws IOException { User
		 * currentUser = payCheckDao.findUserById(id); Map<String, String> map = new
		 * HashMap<String, String>(); String fileType =
		 * FilenameUtils.getExtension(file.getOriginalFilename()); String folder =
		 * "/image/kyc_document/"+imageSide+"/"; if
		 * (currentUser.getTokenId().equals(tokenId)) { if (!file.isEmpty()) { if
		 * (fileType.equals("pdf") || fileType.equals("png") || fileType.equals("jpg")
		 * || fileType.equals("docx") || fileType.equals("doc")) {// png doc //jpg try {
		 * saveImage(file, request, folder, currentUser, fileType); String contextPath =
		 * request.getContextPath(); if(imageSide.equals("front")) {
		 * currentUser.setKycFrontPic(
		 * contextPath.concat(folder).concat(currentUser.getEmailId()) +"." +
		 * fileType);//+"_"+imageSide map.put("kycDocument",
		 * currentUser.getKycFrontPic()); } if(imageSide.equals("back")) {
		 * currentUser.setKycBackPic(
		 * contextPath.concat(folder).concat(currentUser.getEmailId()) +"." +
		 * fileType);//+"_"+imageSide+ map.put("kycDocument",
		 * currentUser.getKycBackPic()); } payCheckDao.updateUser(currentUser);
		 * map.put("status", "success");
		 * 
		 * return map; } catch (Exception e) { map.put("status", "failure");
		 * map.put("message", e.getMessage()); } } else { map.put("status", "failure");
		 * map.put("message", "Not valid format");
		 * 
		 * } } else { map.put("status", "failure"); map.put("message",
		 * "Not selected any files"); } } else { map.put("status", "failure");
		 * map.put("message", "session expired"); }
		 * 
		 * return map; }
		 */
	
		 @RequestMapping(value = "/updateLoanType", method = RequestMethod.POST)
			public @ResponseBody Map<String, Object> updateLoanType(@RequestParam("file") MultipartFile file,
					@RequestParam("loanName") String loanName,HttpServletRequest request
					 ) throws Exception  {
			 Map<String, String> map = new HashMap<String, String>();
			String fileType = FilenameUtils.getExtension(file.getOriginalFilename());
			System.out.println(file.getOriginalFilename());
			if (!file.isEmpty()) {
				if ( fileType.equals("png") || fileType.equals("jpg")){
					String contextPath = request.getContextPath();
					InetAddress IP=InetAddress.getLocalHost();
					
//					System.out.println(IP.getHostAddress());
//					System.out.println(IpAddress.getEndPoints());
//					System.out.println(getIpAddressAndPort());
					String url = getIpAddressAndPort();
//					System.out.println(url);
					try {
						String folder = null;
						LoanType loanType = payCheckDao.findLoanTypeByName(loanName);
//						System.out.println("***************"+loanType);
//						System.out.println("&&&&&&&&&&&");
						if(loanType != null) {
//							System.out.println("*******1111");
							if(loanType.getLoanName().equals("Personnel Loan")) {
//								System.out.println("*******22");
								folder = "/image/Loan/personnel/";
								loanType.setLoanImage(url+contextPath.concat(folder).concat(file.getOriginalFilename()));
								
							}
							else if(loanType.getLoanName().equals("Car Loan")) {
								folder = "/image/Loan/car/";
								loanType.setLoanImage(url+contextPath.concat(folder).concat(file.getOriginalFilename()));
								
							}
							else if(loanType.getLoanName().equals("Home Loan")) {
								folder = "/image/Loan/home/";
								loanType.setLoanImage(url+contextPath.concat(folder).concat(file.getOriginalFilename()));
								
							}
							saveLoanImage(file, request, folder, file.getOriginalFilename());
							payCheckDao.updateLoanType(loanType);
							
						}
						/*if (DocumentTypes.PASSPORT.equals(documentType)) {
							folder = "/image/loan/";
							currentUser.setPassport(
									contextPath.concat(folder).concat(currentUser.getEmailId()) + "." + fileType);
							map.put("passport", currentUser.getPassport());
						}*/
					}catch(Exception e) {
						
					}
					
					
				}
				}
			 
			 
			return null; 
		 }
		 
		 
		 public String getIpAddressAndPort() 
				 throws MalformedObjectNameException, NullPointerException,
				             UnknownHostException {

				         MBeanServer beanServer = ManagementFactory.getPlatformMBeanServer();

				         Set<ObjectName> objectNames = beanServer.queryNames(new ObjectName("*:type=Connector,*"),
				                 Query.match(Query.attr("protocol"), Query.value("HTTP/1.1")));

				         String host = InetAddress.getLocalHost().getHostAddress();
				         String port = objectNames.iterator().next().getKeyProperty("port");
				         System.out.println(objectNames.iterator().next());
				         
//				         String scheme = beanServer.getAttribute(obj, "scheme").toString();
//				         String scheme = beanServer.getAttribute(objectNames.iterator().next().getKeyProperty("http") ,"scheme")).toString();
//				         String ep = "http" + "://" + host + ":" + port;
				         String ep = "http://185.78.163.41:8080";

				         System.out.println("IP Address of System : "+host );
				         System.out.println("port of tomcat server : "+port);
				         return ep;

				     }
		 
		 private void saveLoanImage(MultipartFile file, HttpServletRequest request, String folder,String name) throws IOException {
				HttpSession session = request.getSession();
				ServletContext sc = session.getServletContext();
				File file1 = new File(sc.getRealPath("/") + folder);
				file1.mkdirs();
				byte[] bytes = file.getBytes();
				BufferedOutputStream stream = new BufferedOutputStream(
						new FileOutputStream(new File(file1 + "/" + name)));
				stream.write(bytes);
				stream.close();
				 
			}
		 
		 @RequestMapping(value = "/image", method = RequestMethod.GET)
		 public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response)
		            throws ServletException, IOException {
			 System.out.println("************************8");

		        String now = (new Date()).toString();
//		        logger.info("Returning hello view with " + now);

//		        return new ModelAndView("WEB-INF/emailImage/paychecImage.html", "now", now);
		        return new ModelAndView("WEB-INF/emailImage/paychecImage.html");
//		        return "index.jsp";
		    }
		 

}
