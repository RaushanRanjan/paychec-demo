package com.artivatic.paychec.exception;

public class EmailFoundException  extends Exception {
	 
    public EmailFoundException(String message) {
        super(message);
    }
}
