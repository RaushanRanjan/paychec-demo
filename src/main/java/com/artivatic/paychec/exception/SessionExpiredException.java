package com.artivatic.paychec.exception;

public class SessionExpiredException extends Exception {
	 
    public SessionExpiredException(String message) {
        super(message);
    }
}