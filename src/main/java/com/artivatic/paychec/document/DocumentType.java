package com.artivatic.paychec.document;

public enum DocumentType {
	INSURENCE,
	PAYSLIP,
	PASSPORT,
	PROFILE,
	BANKSTATEMENT,
	CREDITCARD,
	HEALTHCERTIFICATE

}
