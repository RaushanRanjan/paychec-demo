package com.artivatic.paychec.document;

public  final class DocumentTypes {
	
	public static final  String INSURENCE = "insurence";
	public static final  String PASSPORT = "passport";
	public static final  String BANKSTATEMENT = "bankstatement";
	public static final  String HEALTHCERTIFICATE = "health";
	public static final  String CREDITCARD = "credit";
	public static final  String PHOTO = "photo";
	public static final  String NATIONALID = "nationalId";
	public static final  String PAYSLIP = "paySlip";

}
