package com.artivatic.paychec.dao;

import java.util.List;

import com.artivatic.paychec.entity.BankFinanceDetails;
import com.artivatic.paychec.entity.DocumentType;
import com.artivatic.paychec.entity.DocumentImage;
import com.artivatic.paychec.entity.DocumentSubType;
import com.artivatic.paychec.entity.InterestRate;
import com.artivatic.paychec.entity.KycDocumentDetails;
import com.artivatic.paychec.entity.Loan;
import com.artivatic.paychec.entity.LoanType;
import com.artivatic.paychec.entity.PaidLoan;
import com.artivatic.paychec.entity.ResetPassword;
import com.artivatic.paychec.entity.User;

public interface PayCheckDao {

	public Long saveUser(User user);

	public User findUserByEmail(User user);

	public User findUserByPhoneNumber(User user);

	public User findUserByToken(User user);

	public User findUserById(Long id);

	public User findUserByTokenWithId(Long id, String token);

	public void updateUser(User user);

	// public User updateUser(User user);
	public User findMobileNumber(String phoneNumber);

	public List<InterestRate> getAllLoan();

	public List<LoanType> getLoanType();

	public Long saveOtp(ResetPassword resetPassword);

	public void updateOtp(ResetPassword resetPassword);

	public ResetPassword findResetPasswordByEmail(ResetPassword resetPassword);
	// public List<ResetPassword> findResetPassword(ResetPassword resetPassword) ;

	public Long saveLoan(Loan loan);

	public User findUserByfacebookId(String facebookId);

	// public Loan saveLoan(Loan loan);
	public InterestRate findRateById(Long id);

	public Long saveBankFinanceDetails(BankFinanceDetails financeDetails);

	public BankFinanceDetails updateBankFinanceDetails(BankFinanceDetails financeDetails);

	public Long saveKycDocumentDetails(KycDocumentDetails kycDocumentDetails);

	public BankFinanceDetails updateKycDocumentDetails(KycDocumentDetails kycDocumentDetails);

	public List<DocumentImage> findDocumentImageByUser(User user);

	public DocumentImage findDocumentImageByUser(User user, DocumentType dcumentType, DocumentSubType documentSubType);

	public DocumentType findDcumentTypeById(Long id);

	public DocumentSubType findDocumentSubTypeById(Long id);

	public Long saveDocumentImage(DocumentImage documentImage);

	public DocumentImage updateDocumentImage(DocumentImage documentImage);

	public List<DocumentSubType> findDocumentSubTypeByType(DocumentType documentType);

	public List<DocumentType> getAllDocument();

	public DocumentSubType findDocumentSubTypeByName(String name);

	public User mergeUser(User user);

	public LoanType findLoanTypeByName(String name);

	public LoanType updateLoanType(LoanType loanType);

	public LoanType findLoanTypeById(Long id);
	// public LoanType findLoanTypeById(Long id) ;

	public Long savePaidLoan(PaidLoan loan);

	public Loan findLoanById(Long id);
	public Long findTotalBehindUser(User user) ;
	public Long findTotalCount();

}
