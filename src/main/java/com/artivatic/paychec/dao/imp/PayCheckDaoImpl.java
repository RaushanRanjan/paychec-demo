package com.artivatic.paychec.dao.imp;

import java.util.List;

//import javax.persistence.Query;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.artivatic.paychec.dao.PayCheckDao;
import com.artivatic.paychec.entity.BankFinanceDetails;
import com.artivatic.paychec.entity.DocumentType;
import com.artivatic.paychec.entity.DocumentImage;
import com.artivatic.paychec.entity.DocumentSubType;
import com.artivatic.paychec.entity.InterestRate;
import com.artivatic.paychec.entity.KycDocumentDetails;
import com.artivatic.paychec.entity.Loan;
import com.artivatic.paychec.entity.LoanType;
import com.artivatic.paychec.entity.PaidLoan;
import com.artivatic.paychec.entity.ResetPassword;
import com.artivatic.paychec.entity.User;

@Service("payCheckDao")
@Transactional(propagation = Propagation.REQUIRED)
public class PayCheckDaoImpl implements PayCheckDao {

	@Autowired
	private SessionFactory sessionFactory;

	public Long saveUser(User user) {
		// TODO Auto-generated method stub
		return (Long) sessionFactory.getCurrentSession().save(user);
	}

	public User mergeUser(User user) {
		// TODO Auto-generated method stub
		return (User) sessionFactory.getCurrentSession().merge(user);
	}

	public User findUserByEmail(User user) {
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(User.class);
		criteria.add(Restrictions.eq("emailId", user.getEmailId()));
		return (User) criteria.uniqueResult();

	}

	public User findUserByfacebookId(String facebookId) {
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(User.class);
		criteria.add(Restrictions.eq("facebookId", facebookId));
		return (User) criteria.uniqueResult();

	}

	public User findUserByPhoneNumber(User user) {
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(User.class);
		criteria.add(Restrictions.eq("phoneNumber", user.getPhoneNumber()));
		return (User) criteria.uniqueResult();

	}

	public User findUserById(Long id) {
		return (User) sessionFactory.getCurrentSession().get(User.class, id);
	}

	public User findUserByToken(User user) {
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(User.class);
		criteria.add(Restrictions.eq("tokenId", user.getTokenId()));
		return (User) criteria.uniqueResult();
	}

	public User findUserByTokenWithId(Long id, String token) {
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(User.class);
		criteria.add(Restrictions.eq("id", id));
		criteria.add(Restrictions.eq("tokenId", token));
		return (User) criteria.uniqueResult();
	}

	public void updateUser(User user) {

		sessionFactory.getCurrentSession().saveOrUpdate(user);// saveOrUpdate(user)
	}
	public Long findTotalBehindUser(User user) {
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(User.class);
		criteria.add( Restrictions.gt("createdDate", user.getCreatedDate()));
		criteria.add( Restrictions.eq("verified", false));
		criteria.setProjection(Projections.rowCount());
		return (Long) criteria.uniqueResult();
	}
	public Long findTotalCount() {
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(User.class);
//		criteria.add( Restrictions.gt("createdDate", user.getCreatedDate()));
		criteria.setProjection(Projections.rowCount());
		return (Long) criteria.uniqueResult();
	}


	public User findMobileNumber(String phoneNumber) {
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(User.class);
		criteria.add(Restrictions.eq("phoneNumber", phoneNumber));
		return (User) criteria.uniqueResult();
	}

	public List<InterestRate> getAllLoan() {
		Query query = sessionFactory.getCurrentSession().createQuery("select rate from InterestRate rate ");
		return query.list();

	}

	public List<LoanType> getLoanType() {

		Query query = sessionFactory.getCurrentSession().createQuery("select loanType from LoanType loanType ");
		return query.list();

	}

	public Long saveOtp(ResetPassword resetPassword) {
		// TODO Auto-generated method stub
		return (Long) sessionFactory.getCurrentSession().save(resetPassword);
	}

	public void updateOtp(ResetPassword resetPassword) {

		sessionFactory.getCurrentSession().saveOrUpdate(resetPassword);// saveOrUpdate(user)
	}

	public ResetPassword findResetPasswordByEmail(ResetPassword resetPassword) {
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(ResetPassword.class);
		criteria.add(Restrictions.eq("emailId", resetPassword.getEmailId()));
		return (ResetPassword) criteria.uniqueResult();
		// return criteria.list();

	}

	public Long saveLoan(Loan loan) {
		return (Long) sessionFactory.getCurrentSession().save(loan);
	}

	public InterestRate findRateById(Long id) {
		return (InterestRate) sessionFactory.getCurrentSession().get(InterestRate.class, id);

	}

	public Long saveBankFinanceDetails(BankFinanceDetails financeDetails) {
		return (Long) sessionFactory.getCurrentSession().save(financeDetails);
	}

	public BankFinanceDetails updateBankFinanceDetails(BankFinanceDetails financeDetails) {
		return (BankFinanceDetails) sessionFactory.getCurrentSession().merge(financeDetails);
	}

	public Long saveKycDocumentDetails(KycDocumentDetails kycDocumentDetails) {
		return (Long) sessionFactory.getCurrentSession().save(kycDocumentDetails);
	}

	public BankFinanceDetails updateKycDocumentDetails(KycDocumentDetails kycDocumentDetails) {
		return (BankFinanceDetails) sessionFactory.getCurrentSession().merge(kycDocumentDetails);

	}

	@SuppressWarnings("unchecked")
	public List<DocumentImage> findDocumentImageByUser(User user) {

		Query query = sessionFactory.getCurrentSession()
				.createQuery("select di from DocumentImage di where user = :user ").setParameter("user", user);
		return query.list();

	}

	public DocumentImage findDocumentImageByUser(User user, DocumentType dcumentType, DocumentSubType documentSubType) {
		Query query = sessionFactory.getCurrentSession().createQuery(
				"select di from DocumentImage di where user = :user and dcumentType = :dcumentType and documentSubType =:documentSubType  ")
				.setParameter("user", user).setParameter("dcumentType", dcumentType)
				.setParameter("documentSubType", documentSubType);
		return (DocumentImage) query.uniqueResult();
	}

	public DocumentType findDcumentTypeById(Long id) {
		return (DocumentType) sessionFactory.getCurrentSession().get(DocumentType.class, id);
	}

	public DocumentSubType findDocumentSubTypeById(Long id) {
		return (DocumentSubType) sessionFactory.getCurrentSession().get(DocumentSubType.class, id);
	}

	public Long saveDocumentImage(DocumentImage documentImage) {
		return (Long) sessionFactory.getCurrentSession().save(documentImage);

	}

	public DocumentImage updateDocumentImage(DocumentImage documentImage) {
		return (DocumentImage) sessionFactory.getCurrentSession().merge(documentImage);
	}

	public List<DocumentSubType> findDocumentSubTypeByType(DocumentType documentType) {
		Query query = sessionFactory.getCurrentSession()
				.createQuery("select docSubType from DocumentSubType docSubType where documentType = :documentType ")
				.setParameter("documentType", documentType);
		return query.list();

	}

	public List<DocumentType> getAllDocument() {
		Query query = sessionFactory.getCurrentSession()
				.createQuery("select documentType from DocumentType documentType ");
		return query.list();

	}

	public DocumentSubType findDocumentSubTypeByName(String name) {
		Query query = sessionFactory.getCurrentSession()
				.createQuery("select docSubType from DocumentSubType docSubType where name = :name ")
				.setParameter("name", name);
		return (DocumentSubType) query.uniqueResult();

	}

	public LoanType findLoanTypeByName(String name) {
		Query query = sessionFactory.getCurrentSession()
				.createQuery("select loanType from LoanType loanType where loanName = :name ")
				.setParameter("name", name);
		return (LoanType) query.uniqueResult();

	}

	public LoanType updateLoanType(LoanType loanType) {
		return (LoanType) sessionFactory.getCurrentSession().merge(loanType);
	}

	public LoanType findLoanTypeById(Long id) {
		return (LoanType) sessionFactory.getCurrentSession().get(LoanType.class, id);
	}

	public Long savePaidLoan(PaidLoan loan) {
		return (Long) sessionFactory.getCurrentSession().save(loan);
	}

	public Loan findLoanById(Long id) {
		return (Loan) sessionFactory.getCurrentSession().get(Loan.class, id);
	}

}
